package com.example.graduationprogram;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.graduationprogram.adapter.GameArrangeAdapter;
import com.example.graduationprogram.database.DatabaseHelper;
import com.example.graduationprogram.enity.GameItem;
import com.example.graduationprogram.enity.TeamItem;

import java.util.ArrayList;
import java.util.Random;

public class GameDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private ArrayList<TeamItem> team_list;
    private ArrayList<GameItem> game_list;
    private Random random;
    private ArrayList<Integer> selected_sex;
    private ArrayList<String> selected_name;
    private ListView game_arrange;
    private GameArrangeAdapter gameArrangeAdapter;

    private DatabaseHelper mSQLite;
    private String game_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_details);

        game_arrange = findViewById(R.id.game_arrange);

        findViewById(R.id.btn_refresh).setOnClickListener(this);
        findViewById(R.id.btn_save).setOnClickListener(this);

        Toolbar tl_back_to_select = findViewById(R.id.tl_back_to_select);
        tl_back_to_select.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Bundle bundle = this.getIntent().getExtras();
        selected_sex = bundle.getIntegerArrayList("selected_sex");
        selected_name = bundle.getStringArrayList("selected_name");
        game_name = "game_" + selected_name.size() + "_" + bundle.getString("game_name");

        //队伍分组
        team_list = new ArrayList<>();
        for (int i = 1; i < selected_name.size(); i++) {
            for (int j = i; j < selected_name.size(); j++) {
                team_list.add(new TeamItem(selected_name.get(i - 1), selected_name.get(j)));
            }
        }
        //定义随机对象
        random = new Random();
        //开始安排比赛
        ArrangeGame();
        //比赛赛程展示
        for (int i = 0; i < game_list.size(); i++) {
            Log.d("jin", game_list.get(i).getPlayerItem_1()[0] + game_list.get(i).getPlayerItem_1()[1] + "VS" + game_list.get(i).getPlayerItem_2()[0] + game_list.get(i).getPlayerItem_2()[1]);
        }
        gameArrangeAdapter = new GameArrangeAdapter(game_list, GameDetailsActivity.this);
        game_arrange.setAdapter(gameArrangeAdapter);
    }

    private void ArrangeGame() {
        //比赛安排(应随机)
        game_list = new ArrayList<>();
        //队伍两两vs，且每个队伍仅打一次
        while (team_list.size() != 0) {
            //随机挑选队伍1
            int number_1 = random.nextInt(team_list.size());
            TeamItem team_select_1 = team_list.get(number_1);
            team_list.remove(number_1);

            //若队伍数量为单数，则最后一场比赛新添一个队伍（即有两个人多打一次比赛）
            if (team_list.size() == 0) {
                ArrayList<String> player_extra = new ArrayList<>();
                for (int i = 0; i < selected_name.size(); i++) {
                    if (selected_name.get(i) != team_select_1.getTeam_item()[0] && selected_name.get(i) != team_select_1.getTeam_item()[1]) {
                        player_extra.add(selected_name.get(i));
                    }
                }
                //选取第一个额外成员
                int number_extra_1 = random.nextInt(player_extra.size());
                String player_extra_1 = player_extra.get(number_extra_1);
                player_extra.remove(number_extra_1);
                //选取第二个额外成员
                int number_extra_2 = random.nextInt(player_extra.size());
                String player_extra_2 = player_extra.get(number_extra_2);
                //将新选的额外队伍添加到队伍列表team_list中
                team_list.add(new TeamItem(player_extra_1, player_extra_2));
            }
            //随机挑选队伍2
            int number_2 = random.nextInt(team_list.size());
            TeamItem team_select_2 = team_list.get(number_2);
            team_list.remove(number_2);

            //添加两个队伍于一场比赛中
            game_list.add(new GameItem(team_select_1.getTeam_item(), "vs", team_select_2.getTeam_item()));
        }

        //若比赛中出现一场比赛存在两个同样的人，则回调函数，重新分配
        for (int i = 0; i < game_list.size(); i++) {
            if (game_list.get(i).getPlayerItem_1()[0] == game_list.get(i).getPlayerItem_2()[0] ||
                    game_list.get(i).getPlayerItem_1()[0] == game_list.get(i).getPlayerItem_2()[1] ||
                    game_list.get(i).getPlayerItem_1()[1] == game_list.get(i).getPlayerItem_2()[0] ||
                    game_list.get(i).getPlayerItem_1()[1] == game_list.get(i).getPlayerItem_2()[1]) {
                //队伍分组
                team_list = new ArrayList<>();
                for (int j = 1; j < selected_name.size(); j++) {
                    for (int k = j; k < selected_name.size(); k++) {
                        team_list.add(new TeamItem(selected_name.get(j - 1), selected_name.get(k)));
                    }
                }
                //回调函数
                ArrangeGame();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_refresh:
                //队伍分组
                team_list = new ArrayList<>();
                for (int j = 1; j < selected_name.size(); j++) {
                    for (int k = j; k < selected_name.size(); k++) {
                        team_list.add(new TeamItem(selected_name.get(j - 1), selected_name.get(k)));
                    }
                }
                ArrangeGame();
                for (int i = 0; i < game_list.size(); i++) {
                    Log.d("jin", game_list.get(i).getPlayerItem_1()[0] + game_list.get(i).getPlayerItem_1()[1] + "VS" + game_list.get(i).getPlayerItem_2()[0] + game_list.get(i).getPlayerItem_2()[1]);
                }
                gameArrangeAdapter.setGame_list(game_list);
                gameArrangeAdapter.notifyDataSetChanged();
                break;
            case R.id.btn_save:
                //Log.d("jin",game_name);
                mSQLite = new DatabaseHelper(GameDetailsActivity.this);
                mSQLite.createNewTable("create table " + game_name + "(_id integer, player_1 varchar(40), player_2 varchar(40),player_3 varchar(40),player_4 varchar(40),is_finish integer,score_1 integer,score_2 integer)", game_name);
                for (int i = 0; i < game_list.size(); i++) {
                    mSQLite.insert_game(game_name, i, game_list.get(i).getPlayerItem_1()[0], game_list.get(i).getPlayerItem_1()[1], game_list.get(i).getPlayerItem_2()[0], game_list.get(i).getPlayerItem_2()[1], 0, 0, 0);
                }
                Intent intent = new Intent(GameDetailsActivity.this, TabGroupActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
        }
    }
}