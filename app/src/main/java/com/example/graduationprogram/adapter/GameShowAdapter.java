package com.example.graduationprogram.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.graduationprogram.R;
import com.example.graduationprogram.enity.GameIsFinish;
import com.example.graduationprogram.enity.GameItemShow;

import java.util.ArrayList;

public class GameShowAdapter extends BaseAdapter {

    private Context context;

    public void setAllGame(ArrayList<ArrayList<GameItemShow>> allGame) {
        this.allGame = allGame;
    }

    private ArrayList<ArrayList<GameItemShow>> allGame;

    public void setGame_is_finish(ArrayList<GameIsFinish> game_is_finish) {
        this.game_is_finish = game_is_finish;
    }

    private ArrayList<GameIsFinish> game_is_finish;
    private int progress_value;
    private GameShowAdapter.OnItemClickListener listener;
    private GameShowAdapter.OnItemLongClickListener long_listener;


    public GameShowAdapter(Context context, ArrayList<ArrayList<GameItemShow>> allGame,ArrayList<GameIsFinish> game_is_finish,GameShowAdapter.OnItemClickListener listener,GameShowAdapter.OnItemLongClickListener long_listener) {
        this.context = context;
        this.allGame = allGame;
        this.game_is_finish = game_is_finish;
        this.listener = listener;
        this.long_listener = long_listener;
    }

    public interface OnItemClickListener {
        void onClick(int pos);
    }

    public interface OnItemLongClickListener {
        void onLongClick(int pos);
    }

    @Override
    public int getCount() {
        return allGame.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout layout_game_show = new LinearLayout(context);
        layout_game_show.setOrientation(LinearLayout.VERTICAL);
        layout_game_show.setVerticalGravity(Gravity.CENTER);
        layout_game_show.setHorizontalGravity(Gravity.CENTER);
        layout_game_show.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,360));
        layout_game_show.setBackgroundResource(R.drawable.game_list_shadow);

        TextView tv_game_name = new TextView(context);
        tv_game_name.setText(allGame.get(position).get(0).getGame_name().split("_",3)[2]);
        tv_game_name.setTextSize(20);
        tv_game_name.setPadding(40,0,0,15);

        ProgressBar pb_finish = new ProgressBar(context,null,android.R.attr.progressBarStyleHorizontal);
        pb_finish.setLayoutParams(new LinearLayout.LayoutParams(1000,80));
        pb_finish.setIndeterminate(false);
        pb_finish.setProgressDrawable(context.getResources().getDrawable(android.R.drawable.progress_horizontal));
        progress_value = pb_finish.getProgress();
        progress_value = 100 * game_is_finish.get(position).getFinished_number() / game_is_finish.get(position).getAll_number();
        //progress_value = 20;
        pb_finish.setProgress(progress_value);

        LinearLayout layout_data = new LinearLayout(context);
        layout_data.setOrientation(LinearLayout.HORIZONTAL);
        layout_data.setHorizontalGravity(Gravity.CENTER);
        layout_data.setVerticalGravity(Gravity.CENTER);
        layout_data.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,160));

        LinearLayout layout_data_1 = new LinearLayout(context);
        layout_data_1.setOrientation(LinearLayout.VERTICAL);
        layout_data_1.setHorizontalGravity(Gravity.CENTER);

        TextView tv_player_number = new TextView(context);
        tv_player_number.setText("参赛人数");
        tv_player_number.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tv_player_number.setTextSize(18);
        tv_player_number.setTextColor(Color.BLACK);
        TextView tv_player_number_ = new TextView(context);
        tv_player_number_.setText(allGame.get(position).get(0).getPlayer_number()+"人");
        tv_player_number_.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tv_player_number_.setTextSize(16);
        tv_player_number_.setTextColor(Color.BLACK);

        layout_data_1.addView(tv_player_number);
        layout_data_1.addView(tv_player_number_);

        LinearLayout layout_data_2 = new LinearLayout(context);
        layout_data_2.setOrientation(LinearLayout.VERTICAL);
        layout_data_2.setHorizontalGravity(Gravity.CENTER);
        layout_data_2.setVerticalGravity(Gravity.CENTER);
        layout_data_2.setLayoutParams(new LinearLayout.LayoutParams(600, 160));

        TextView tv_finish_number = new TextView(context);
        tv_finish_number.setText("完成场数");
        tv_finish_number.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tv_finish_number.setTextSize(18);
        tv_finish_number.setTextColor(Color.BLACK);
        TextView tv_finish_number_ = new TextView(context);
        tv_finish_number_.setText(game_is_finish.get(position).getFinished_number()+"场");
        tv_finish_number_.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tv_finish_number_.setTextSize(16);
        tv_finish_number_.setTextColor(Color.BLUE);

        layout_data_2.addView(tv_finish_number);
        layout_data_2.addView(tv_finish_number_);

        LinearLayout layout_data_3 = new LinearLayout(context);
        layout_data_3.setOrientation(LinearLayout.VERTICAL);
        layout_data_3.setHorizontalGravity(Gravity.CENTER);

        TextView tv_all_number = new TextView(context);
        tv_all_number.setText("总场数");
        tv_all_number.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tv_all_number.setTextSize(18);
        tv_all_number.setTextColor(Color.BLACK);
        TextView tv_all_number_ = new TextView(context);
        tv_all_number_.setText(game_is_finish.get(position).getAll_number()+"场");
        tv_all_number_.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tv_all_number_.setTextSize(16);
        tv_all_number_.setTextColor(Color.BLACK);

        layout_data_3.addView(tv_all_number);
        layout_data_3.addView(tv_all_number_);

        layout_data.addView(layout_data_1);
        layout_data.addView(layout_data_2);
        layout_data.addView(layout_data_3);

        layout_game_show.addView(tv_game_name);
        layout_game_show.addView(pb_finish);
        layout_game_show.addView(layout_data);

        layout_game_show.setOnClickListener(v -> listener.onClick(position));
        layout_game_show.setOnLongClickListener(v -> {
            long_listener.onLongClick(position);
            return false;
        });

        return layout_game_show;
    }
}
