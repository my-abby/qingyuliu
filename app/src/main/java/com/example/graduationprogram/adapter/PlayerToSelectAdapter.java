package com.example.graduationprogram.adapter;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class PlayerToSelectAdapter extends BaseAdapter {

    private ArrayList<Integer> listSexArray = new ArrayList<>();
    private ArrayList<String> listNameArray = new ArrayList<>();
    private Context context;
    private PlayerToSelectAdapter.OnItemClickListener listener;

    public PlayerToSelectAdapter(ArrayList<Integer> listSexArray, ArrayList<String> listNameArray, Context context, PlayerToSelectAdapter.OnItemClickListener listener) {
        this.listSexArray = listSexArray;
        this.listNameArray = listNameArray;
        this.context = context;
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onClick(int pos);
    }

    @Override
    public int getCount() {
        return listNameArray.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout layout_player_to_select = new LinearLayout(context);
        layout_player_to_select.setOrientation(LinearLayout.HORIZONTAL);
        layout_player_to_select.setVerticalGravity(Gravity.CENTER);
        layout_player_to_select.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,150));
        layout_player_to_select.setPadding(10,0,0,10);

        ImageView iv_sex = new ImageView(context);
        iv_sex.setImageResource(listSexArray.get(position));

        TextView tv_name = new TextView(context);
        tv_name.setText(listNameArray.get(position));
        tv_name.setPadding(20,0,0,0);
        tv_name.setTextSize(20);

        layout_player_to_select.addView(iv_sex);
        layout_player_to_select.addView(tv_name);
        layout_player_to_select.setOnClickListener(v -> listener.onClick(position));

        return layout_player_to_select;
    }
}
