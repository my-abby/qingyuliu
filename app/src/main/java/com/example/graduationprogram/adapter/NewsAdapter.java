package com.example.graduationprogram.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.graduationprogram.util.GlideUtils;

import java.util.ArrayList;

public class NewsAdapter extends BaseAdapter {

    private ArrayList<String> news_content;
    private Context context;

    public NewsAdapter(ArrayList<String> news_content, Context context) {
        this.news_content = news_content;
        this.context = context;
    }


    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public Object getItem(int position) {
        return news_content.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout layout_content = new LinearLayout(context);
        layout_content.setOrientation(LinearLayout.VERTICAL);

        TextView tv_title = new TextView(context);
        tv_title.setText(news_content.get(0));
        tv_title.setTextSize(30);
        tv_title.setTextColor(Color.BLACK);
        tv_title.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tv_title.setPadding(30,0,30,20);
        layout_content.addView(tv_title);

        for (int i = 1; i < news_content.size(); i++) {
            String[] tag = news_content.get(i).split(":", 2);
            if (tag[0].equals("https")) {
                //Log.d("jin",news_content.get(i));
                ImageView iv_pic = new ImageView(context);
                GlideUtils.getInstance().ImageLoader(context, news_content.get(i), iv_pic, 1080, 520);
                iv_pic.setPadding(0,20,0,20);
                layout_content.addView(iv_pic);
            } else {
                TextView tv_content = new TextView(context);
                tv_content.setText("        "+news_content.get(i));
                tv_content.setTextSize(18);
                tv_content.setPadding(30,20,30,20);
                layout_content.addView(tv_content);
            }
        }

        return layout_content;
    }
}
