package com.example.graduationprogram.adapter;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.graduationprogram.R;
import com.example.graduationprogram.enity.User;
import com.example.graduationprogram.enity.UserInfo;

import java.util.ArrayList;

public class LinearAdapter extends RecyclerView.Adapter<LinearAdapter.LinearViewHolder> {
    private Context w_context;
    private OnItemClickListener c_listener;
    private OnItemLongClickListener lc_listener;

    public void setmMemberArray(ArrayList<UserInfo> mMemberArray) {
        this.mMemberArray = mMemberArray;
    }

    private ArrayList<UserInfo> mMemberArray;

    public LinearAdapter(Context context, OnItemClickListener c_listener, OnItemLongClickListener lc_listener,ArrayList<UserInfo> MemberArray) {
        this.c_listener = c_listener;
        this.w_context = context;
        this.lc_listener = lc_listener;
        this.mMemberArray = MemberArray;
    }

    class LinearViewHolder extends RecyclerView.ViewHolder {
        private TextView textView;
        private ImageView imageView;

        public LinearViewHolder(@NonNull View itemView) {
            super(itemView);
            //找到我们刚刚设置的子元素的效果中的两个控件
            textView = itemView.findViewById(R.id.tv_name);
            imageView = itemView.findViewById(R.id.iv_sex);

        }
    }
    @NonNull
    @Override
    public LinearViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new LinearViewHolder(LayoutInflater.from(w_context).inflate(R.layout.layout_linear_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull LinearAdapter.LinearViewHolder holder, int position) {
        holder.textView.setText(mMemberArray.get(position).name);             //设置需要显示的文字
        holder.imageView.setImageResource(mMemberArray.get(position).sex); //设置需要显示的性别图标
        holder.itemView.setOnClickListener(new View.OnClickListener() {             //轻点
            @Override
            public void onClick(View v) {
                c_listener.onClick(position);
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {   //长按
            @Override
            public boolean onLongClick(View v) {
                lc_listener.onLongClick(position);
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mMemberArray.size();     //设置子元素的数量
    }

    public interface OnItemClickListener {
        void onClick(int pos);
    }

    public interface OnItemLongClickListener {
        void onLongClick(int pos);
    }
}
