package com.example.graduationprogram.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.graduationprogram.R;
import com.example.graduationprogram.enity.GameItem;

import java.util.ArrayList;

public class GameArrangeAdapter extends BaseAdapter {

    public void setGame_list(ArrayList<GameItem> game_list) {
        this.game_list = game_list;
    }

    public GameArrangeAdapter(ArrayList<GameItem> game_list, Context context) {
        this.game_list = game_list;
        this.context = context;
    }

    private ArrayList<GameItem> game_list;
    private Context context;

    @Override
    public int getCount() {
        return game_list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int game_id = position + 1;

        LinearLayout layout_game_item = new LinearLayout(context);
        layout_game_item.setOrientation(LinearLayout.VERTICAL);
        layout_game_item.setPadding(30, 40, 30, 40);
        layout_game_item.setBackgroundResource(R.drawable.details_shadow);

        LinearLayout layout_title = new LinearLayout(context);
        layout_title.setOrientation(LinearLayout.HORIZONTAL);
        layout_title.setVerticalGravity(Gravity.CENTER);
        ImageView iv_title = new ImageView(context);
        iv_title.setImageResource(R.drawable.game_pic);
        TextView tv_title = new TextView(context);
        tv_title.setText("第" + game_id + "场");
        tv_title.setTextSize(20);
        layout_title.addView(iv_title);
        layout_title.addView(tv_title);

        LinearLayout layout_content = new LinearLayout(context);
        layout_content.setOrientation(LinearLayout.HORIZONTAL);
        layout_content.setPadding(26, 10, 26, 10);
        layout_content.setHorizontalGravity(Gravity.CENTER);
        layout_content.setVerticalGravity(Gravity.CENTER);
        layout_content.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 120));

        LinearLayout layout_1 = new LinearLayout(context);
        layout_1.setGravity(Gravity.CENTER);
        TextView tv_player_1 = new TextView(context);
        tv_player_1.setText(game_list.get(position).getPlayerItem_1()[0]);
        tv_player_1.setTextSize(16);
        tv_player_1.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tv_player_1.setLayoutParams(new LinearLayout.LayoutParams(180, ViewGroup.LayoutParams.WRAP_CONTENT));
        layout_1.addView(tv_player_1);
        layout_1.setLayoutParams(new LinearLayout.LayoutParams(210, 120));

        LinearLayout layout_2 = new LinearLayout(context);
        layout_2.setGravity(Gravity.CENTER);
        TextView tv_player_2 = new TextView(context);
        tv_player_2.setText(game_list.get(position).getPlayerItem_1()[1]);
        tv_player_2.setTextSize(16);
        tv_player_2.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tv_player_2.setLayoutParams(new LinearLayout.LayoutParams(180, ViewGroup.LayoutParams.WRAP_CONTENT));
        layout_2.addView(tv_player_2);
        layout_2.setLayoutParams(new LinearLayout.LayoutParams(210, 120));

        TextView tv_vs = new TextView(context);
        tv_vs.setText(game_list.get(position).getVs());
        tv_vs.setTextSize(26);
        tv_vs.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        tv_vs.setPadding(35, 0, 35, 0);
        tv_vs.setTextColor(Color.BLUE);

        LinearLayout layout_3 = new LinearLayout(context);
        layout_3.setGravity(Gravity.CENTER);
        TextView tv_player_3 = new TextView(context);
        tv_player_3.setText(game_list.get(position).getPlayerItem_2()[0]);
        tv_player_3.setTextSize(16);
        tv_player_3.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tv_player_3.setLayoutParams(new LinearLayout.LayoutParams(180, ViewGroup.LayoutParams.WRAP_CONTENT));
        layout_3.addView(tv_player_3);
        layout_3.setLayoutParams(new LinearLayout.LayoutParams(210, 120));

        LinearLayout layout_4 = new LinearLayout(context);
        layout_4.setGravity(Gravity.CENTER);
        TextView tv_player_4 = new TextView(context);
        tv_player_4.setText(game_list.get(position).getPlayerItem_2()[1]);
        tv_player_4.setTextSize(16);
        tv_player_4.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        tv_player_4.setLayoutParams(new LinearLayout.LayoutParams(180, ViewGroup.LayoutParams.WRAP_CONTENT));
        layout_4.addView(tv_player_4);
        layout_4.setLayoutParams(new LinearLayout.LayoutParams(210, 120));

        layout_content.addView(layout_1);
        layout_content.addView(layout_2);
        layout_content.addView(tv_vs);
        layout_content.addView(layout_3);
        layout_content.addView(layout_4);

        layout_game_item.addView(layout_title);
        layout_game_item.addView(layout_content);

        return layout_game_item;
    }
}
