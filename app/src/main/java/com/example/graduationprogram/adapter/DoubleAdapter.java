package com.example.graduationprogram.adapter;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.graduationprogram.R;
import com.example.graduationprogram.enity.DoubleRanks;
import com.example.graduationprogram.util.GlideUtils;

import java.util.List;

public class DoubleAdapter extends BaseAdapter {
    private List<DoubleRanks> RanksList;
    private Context context;

    public DoubleAdapter(List<DoubleRanks> ranksList, Context context) {
        RanksList = ranksList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return RanksList.size();
    }

    @Override
    public Object getItem(int i) {
        return RanksList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    //添加JSON数据对应出来的TextView，显示到主界面，非主要代码
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LinearLayout layout_ranks = new LinearLayout(context);
        layout_ranks.setOrientation(LinearLayout.HORIZONTAL);
        layout_ranks.setVerticalGravity(Gravity.CENTER);
        layout_ranks.setPadding(20, 0, 0, 0);
        layout_ranks.setBackgroundResource(R.drawable.list_view_shadow);
        //排名
        LinearLayout layout_rank = new LinearLayout(context);
        layout_rank.setGravity(Gravity.CENTER);
        TextView textViewRank = new TextView(context);
        textViewRank.setText(RanksList.get(i).getRank() + "\t");
        textViewRank.setTextSize(30);
        layout_rank.addView(textViewRank);
        layout_rank.setLayoutParams(new LinearLayout.LayoutParams(80, 160));
        //国家
        LinearLayout layout_country = new LinearLayout(context);
        layout_country.setOrientation(LinearLayout.VERTICAL);
        layout_country.setGravity(Gravity.CENTER);
        layout_country.setPadding(20, 0, 20, 0);
        TextView textViewNameCountry_1 = new TextView(context);
        textViewNameCountry_1.setText(RanksList.get(i).getName_country() + "\t");
        textViewNameCountry_1.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        TextView textViewNameCountry_2 = new TextView(context);
        textViewNameCountry_2.setText(RanksList.get(i).getName_country() + "\t");
        textViewNameCountry_2.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        ImageView imageViewPicCountry_1 = new ImageView(context);
        GlideUtils.getInstance().ImageLoader(context, "https://www.badmintoncn.com" + RanksList.get(i).getPic_country(), imageViewPicCountry_1, 160, 80);
        ImageView imageViewPicCountry_2 = new ImageView(context);
        GlideUtils.getInstance().ImageLoader(context, "https://www.badmintoncn.com" + RanksList.get(i).getPic_country(), imageViewPicCountry_2, 160, 80);
        layout_country.addView(textViewNameCountry_1);
        layout_country.addView(imageViewPicCountry_1);
        layout_country.addView(textViewNameCountry_2);
        layout_country.addView(imageViewPicCountry_2);
        layout_country.setLayoutParams(new LinearLayout.LayoutParams(160, 320));

        LinearLayout layout_pic_player = new LinearLayout(context);
        layout_pic_player.setOrientation(LinearLayout.VERTICAL);
        ImageView imageViewPicPlayer_1 = new ImageView(context);
        GlideUtils.getInstance().ImageLoader(context, "https://www.badmintoncn.com" + RanksList.get(i).getPic_player_1(), imageViewPicPlayer_1, 160, 160);
        ImageView imageViewPicPlayer_2 = new ImageView(context);
        GlideUtils.getInstance().ImageLoader(context, "https://www.badmintoncn.com" + RanksList.get(i).getPic_player_2(), imageViewPicPlayer_2, 160 ,160);
        layout_pic_player.addView(imageViewPicPlayer_1);
        layout_pic_player.addView(imageViewPicPlayer_2);
        layout_pic_player.setLayoutParams(new LinearLayout.LayoutParams(160,320));

        LinearLayout layout_name = new LinearLayout(context);
        layout_name.setOrientation(LinearLayout.VERTICAL);
        layout_name.setGravity(Gravity.CENTER);
        layout_name.setLayoutParams(new LinearLayout.LayoutParams(480,320));
        TextView textViewNamePlayerCh_1 = new TextView(context);
        textViewNamePlayerCh_1.setText(RanksList.get(i).getName_player_ch_1() + "\t");
        textViewNamePlayerCh_1.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        TextView textViewNamePlayerCh_2 = new TextView(context);
        textViewNamePlayerCh_2.setText(RanksList.get(i).getName_player_ch_2() + "\t");
        textViewNamePlayerCh_2.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        TextView textViewNamePlayerEn_1 = new TextView(context);
        textViewNamePlayerEn_1.setText(RanksList.get(i).getName_player_en_1() + "\t");
        textViewNamePlayerEn_1.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        TextView textViewNamePlayerEn_2 = new TextView(context);
        textViewNamePlayerEn_2.setText(RanksList.get(i).getName_player_en_2() + "\t");
        textViewNamePlayerEn_2.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        layout_name.addView(textViewNamePlayerCh_1);
        layout_name.addView(textViewNamePlayerEn_1);
        layout_name.addView(textViewNamePlayerCh_2);
        layout_name.addView(textViewNamePlayerEn_2);

        LinearLayout layout_rank_change = new LinearLayout(context);
        layout_rank_change.setOrientation(LinearLayout.VERTICAL);
        layout_rank_change.setVerticalGravity(Gravity.CENTER);
        TextView textViewRankChange = new TextView(context);
        textViewRankChange.setText(RanksList.get(i).getRank_change() + "\t");
        textViewRankChange.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        ImageView imageViewRankUpOrDown = new ImageView(context);
        GlideUtils.getInstance().ImageLoader(context, "https://www.badmintoncn.com" + RanksList.get(i).getRank_up_or_down(), imageViewRankUpOrDown, 20, 20);
        layout_rank_change.addView(textViewRankChange);
        layout_rank_change.addView(imageViewRankUpOrDown);
        layout_rank_change.setLayoutParams(new LinearLayout.LayoutParams(80,160));

        TextView textViewScore = new TextView(context);
        textViewScore.setText(RanksList.get(i).getScore() + "\t");


        layout_ranks.addView(layout_rank);
        layout_ranks.addView(layout_country);
        layout_ranks.addView(layout_pic_player);
        layout_ranks.addView(layout_name);
        layout_ranks.addView(layout_rank_change);
        layout_ranks.addView(textViewScore);

        return layout_ranks;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }
}

