package com.example.graduationprogram.adapter;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class PlayerSelectedAdapter extends BaseAdapter {

    private ArrayList<Integer> listSexSelected;
    private ArrayList<String> listNameSelected;
    private Context context;
    private PlayerSelectedAdapter.OnItemClickListener listener;

    public PlayerSelectedAdapter(ArrayList<Integer> listSexSelected, ArrayList<String> listNameSelected, Context context, OnItemClickListener listener) {
        this.listSexSelected = listSexSelected;
        this.listNameSelected = listNameSelected;
        this.context = context;
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onClick(int pos);
    }

    @Override
    public int getCount() {
        return listNameSelected.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout layout_player_selected = new LinearLayout(context);
        layout_player_selected.setOrientation(LinearLayout.HORIZONTAL);
        layout_player_selected.setVerticalGravity(Gravity.CENTER);
        layout_player_selected.setPadding(20,0,0,0);

        ImageView iv_sex_selected = new ImageView(context);
        iv_sex_selected.setImageResource(listSexSelected.get(position));

        TextView tv_name_selected = new TextView(context);
        tv_name_selected.setText(listNameSelected.get(position));
        tv_name_selected.setPadding(20,0,0,0);
        tv_name_selected.setTextSize(15);

        layout_player_selected.addView(iv_sex_selected);
        layout_player_selected.addView(tv_name_selected);

        layout_player_selected.setOnClickListener(v -> listener.onClick(position));

        return layout_player_selected;
    }
}
