package com.example.graduationprogram.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.graduationprogram.fragment.DoubleFragment;
import com.example.graduationprogram.fragment.SingleFragment;

import java.util.ArrayList;

/*viewpager加载fragment的时候使用，viewpager的pageradapter适配器*/
public class RankFragmentAdapter extends FragmentPagerAdapter {
    //声明标题文本队列
    private ArrayList<String> list;
    private String path = "https://www.badmintoncn.com/ranking.php?rw=&type=6";

    //碎片页适配器的构造函数，传入碎片管理器与标题队列
    public RankFragmentAdapter(@NonNull FragmentManager fm, ArrayList<String> list) {
        super(fm);
        this.list = list;
    }

    //获取指定位置的碎片fragment
    @NonNull
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            path = "https://www.badmintoncn.com/ranking.php?rw=&type=6";
            return new SingleFragment(path);
        } else if (position == 1) {
            path = "https://www.badmintoncn.com/ranking.php?rw=&type=7";
            return new SingleFragment(path);
        } else if (position == 2) {
            path = "https://www.badmintoncn.com/ranking.php?rw=&type=8";
            return new DoubleFragment(path);
        } else if (position == 3) {
            path = "https://www.badmintoncn.com/ranking.php?rw=&type=9";
            return new DoubleFragment(path);
        } else if (position == 4) {
            path = "https://www.badmintoncn.com/ranking.php?rw=&type=10";
            return new DoubleFragment(path);
        }
        return new SingleFragment(path);
    }

    //获取fragment的个数
    @Override
    public int getCount() {
        return list.size();
    }

    //获取指定碎片页的标题文本
    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return list.get(position);
    }
}
