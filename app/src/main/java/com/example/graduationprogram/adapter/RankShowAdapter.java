package com.example.graduationprogram.adapter;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.graduationprogram.R;
import com.example.graduationprogram.enity.User;
import com.example.graduationprogram.util.GlideUtils;

import java.util.ArrayList;

public class RankShowAdapter extends BaseAdapter {
    private ArrayList<User> data;
    private Context context;

    public RankShowAdapter(ArrayList<User> data, Context context) {
        this.data = data;
        this.context = context;
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout layout_rank_show = new LinearLayout(context);
        layout_rank_show.setOrientation(LinearLayout.HORIZONTAL);
        layout_rank_show.setVerticalGravity(Gravity.CENTER);
        layout_rank_show.setPadding(20, 0, 0, 0);
        layout_rank_show.setBackgroundResource(R.drawable.list_view_shadow);

        LinearLayout layout_rank = new LinearLayout(context);
        layout_rank.setVerticalGravity(Gravity.CENTER);
        layout_rank.setHorizontalGravity(Gravity.RIGHT);
        TextView tv_rank = new TextView(context);
        tv_rank.setText(position+1+"");
        tv_rank.setTextSize(30);
        layout_rank.addView(tv_rank);
        layout_rank.setLayoutParams(new LinearLayout.LayoutParams(100, 160));

        LinearLayout layout_sex = new LinearLayout(context);
        layout_sex.setHorizontalGravity(Gravity.RIGHT);
        layout_sex.setVerticalGravity(Gravity.CENTER);
        ImageView iv_sex = new ImageView(context);
        if (data.get(position).getSex() == 1){
            iv_sex.setImageResource(R.drawable.male);
        }else {
            iv_sex.setImageResource(R.drawable.female);
        }
        layout_sex.addView(iv_sex);
        layout_sex.setLayoutParams(new LinearLayout.LayoutParams(200,160));


        LinearLayout layout_name = new LinearLayout(context);
        layout_name.setGravity(Gravity.CENTER);
        TextView tv_name = new TextView(context);
        tv_name.setText(data.get(position).getName());
        tv_name.setTextSize(20);
        layout_name.addView(tv_name);
        layout_name.setLayoutParams(new LinearLayout.LayoutParams(160,160));


        LinearLayout layout_points = new LinearLayout(context);
        layout_points.setGravity(Gravity.CENTER);
        TextView tv_points = new TextView(context);
        tv_points.setText(data.get(position).getPoints()+"");
        tv_points.setTextSize(20);
        layout_points.addView(tv_points);
        layout_points.setLayoutParams(new LinearLayout.LayoutParams(120,160));


        LinearLayout layout_won_number = new LinearLayout(context);
        layout_won_number.setGravity(Gravity.CENTER);
        TextView tv_game_won_number = new TextView(context);
        tv_game_won_number.setText(data.get(position).getGame_won_number()+"");
        tv_game_won_number.setTextSize(20);
        layout_won_number.addView(tv_game_won_number);
        layout_won_number.setLayoutParams(new LinearLayout.LayoutParams(160,160));

        LinearLayout layout_number = new LinearLayout(context);
        layout_number.setGravity(Gravity.CENTER);
        TextView tv_game_number = new TextView(context);
        tv_game_number.setText(data.get(position).getGame_number()+"");
        tv_game_number.setTextSize(20);
        layout_number.addView(tv_game_number);
        layout_number.setLayoutParams(new LinearLayout.LayoutParams(120,160));

        LinearLayout layout_won_ratio = new LinearLayout(context);
        layout_won_ratio.setGravity(Gravity.CENTER);
        TextView tv_won_ratio = new TextView(context);
        String ratio = String.format("%.2f",100*(float)data.get(position).getGame_won_number()/(float) data.get(position).getGame_number());
        if (!Float.isNaN((float)data.get(position).getGame_won_number()/(float) data.get(position).getGame_number())){
            tv_won_ratio.setText(ratio+"%");
        }else {
            tv_won_ratio.setText(ratio+"");
        }

        tv_won_ratio.setTextSize(20);
        layout_won_ratio.addView(tv_won_ratio);
        layout_won_ratio.setLayoutParams(new LinearLayout.LayoutParams(160,160));

        layout_rank_show.addView(layout_rank);
        layout_rank_show.addView(layout_sex);
        layout_rank_show.addView(layout_name);
        layout_rank_show.addView(layout_points);
        layout_rank_show.addView(layout_won_number);
        layout_rank_show.addView(layout_number);
        layout_rank_show.addView(layout_won_ratio);

        return layout_rank_show;
    }
}
