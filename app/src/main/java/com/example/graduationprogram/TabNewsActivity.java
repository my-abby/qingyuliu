package com.example.graduationprogram;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.example.graduationprogram.adapter.RankFragmentAdapter;
import com.example.graduationprogram.util.GlideUtils;
import com.example.graduationprogram.util.NewsData;
import com.example.graduationprogram.util.ToastUtil;
import com.example.graduationprogram.views.LooperPager;
import com.example.graduationprogram.enity.NewsItem;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;


public class TabNewsActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {

    private List<NewsItem> mData = new ArrayList<>();
    private LooperPager looper_pager;

    private TabLayout tab_title;
    private ViewPager vp_pager;
    private ArrayList<String> list = new ArrayList<>();
    private String path_news;
    private ArrayList<String> news_content = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_news);

        initData();
        initView();
        initEvent();

        androidx.appcompat.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        //替换系统自带的toolbar
        setSupportActionBar(toolbar);
        list.add("男子单打");
        list.add("女子单打");
        list.add("男子双打");
        list.add("女子双打");
        list.add("混合双打");
        initTabLayout();
        //初始化标签翻页
        initTabViewpager();
    }

    private void initTabViewpager() {
        vp_pager = findViewById(R.id.vp_pager);
        //构建排名翻页的适配器
        RankFragmentAdapter pagerAdapter = new RankFragmentAdapter(getSupportFragmentManager(), list);
        //设置排名翻页适配器
        vp_pager.setAdapter(pagerAdapter);
        //添加页面变更监听器
        vp_pager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                //选中tab_title指定的标签
                tab_title.getTabAt(position).select();
            }
        });
    }

    private void initTabLayout() {
        tab_title = findViewById(R.id.tab_title);
        //添加指定的文字标签
        for (int i = 0; i < list.size(); i++) {
            tab_title.addTab(tab_title.newTab().setText(list.get(i)));
        }
        //标签选中监听器
        tab_title.addOnTabSelectedListener(this);
    }

    //在标签选中时触发
    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        //让viewpager显示指定位置的页面
        vp_pager.setCurrentItem(tab.getPosition());
    }

    //在标签取消选中时触发
    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    //在标签被重复选中时触发
    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    private void initEvent() {
        if (innerAdapter != null) {
            innerAdapter.setPagerItemClickListener(new LooperPager.InnerAdapter.OnPagerItemClickListener() {
                @Override
                public void onItemClick(int position) {
                    switch (position){
                        case 0:
                            news_content.clear();
                            path_news = "https://www.badmintoncn.com/view-26281-1.html";
                            NewsData.DownNewsData(TabNewsActivity.this,path_news,news_content,position);
                            break;
                        case 1:
                            news_content.clear();
                            path_news = "https://www.badmintoncn.com/view-26218-1.html";
                            NewsData.DownNewsData(TabNewsActivity.this,path_news,news_content,position);
                            break;
                        case 2:
                            news_content.clear();
                            path_news = "https://www.badmintoncn.com/view-26081-1.html";
                            NewsData.DownNewsData(TabNewsActivity.this,path_news,news_content,position);
                            break;
                        case 3:
                            news_content.clear();
                            path_news = "https://www.badmintoncn.com/view-25974-1.html";
                            NewsData.DownNewsData(TabNewsActivity.this,path_news,news_content,position);
                            break;
                    }
                    //ToastUtil.show(TabNewsActivity.this, "点击了第" + position + "个item");
                }
            });
        }
    }

    private void initData() {
        mData.add(new NewsItem("奥尔良大师赛丨陈柏阳/刘毅男双夺冠", "https://p6.itc.cn/q_70/images01/20230409/e6b4ab6085674e0496f46e478159ccf1.jpeg"));
        mData.add(new NewsItem("西班牙赛国羽2金1银 何济霆/周昊东登顶", "https://p6.itc.cn/images01/20230403/1ae731adeeaa44c392e1314ac0648214.jpg"));
        mData.add(new NewsItem("全英赛｜李诗沣胜石宇奇称王 雅思登顶 国羽2金2银", "https://p9.itc.cn/q_70/images03/20230320/b51ab509de004e9fbab677ccffda1636.jpg"));
        mData.add(new NewsItem("德国公开赛 冯彦哲/黄东萍夺金 李诗沣不敌伍家朗", "https://p9.itc.cn/q_70/images03/20230313/2e5dba400fde41eaad597e080384a773.jpg"));
    }

    private LooperPager.InnerAdapter innerAdapter = new LooperPager.InnerAdapter() {

        @Override
        protected int getDataSize() {
            return mData.size();
        }

        @Override
        protected View getSubView(ViewGroup container, int position) {
            ImageView iv = new ImageView(container.getContext());
            GlideUtils.getInstance().ImageLoader(TabNewsActivity.this, mData.get(position).getPath(), iv, 600, 200);
            /*iv.setImageResource(mData.get(position).getPicResId());*/
            iv.setScaleType(ImageView.ScaleType.FIT_XY);
            return iv;
        }
    };

    private void initView() {
        looper_pager = findViewById(R.id.looper_pager);
        looper_pager.setData(innerAdapter, new LooperPager.BindTitleListener() {
            @Override
            public String getTitle(int position) {
                return mData.get(position).getTitle();
            }
        });
    }

}
