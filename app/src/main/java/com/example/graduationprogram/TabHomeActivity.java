package com.example.graduationprogram;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import androidx.appcompat.app.AppCompatActivity;

import com.example.graduationprogram.adapter.GameShowAdapter;
import com.example.graduationprogram.database.DatabaseHelper;
import com.example.graduationprogram.enity.GameIsFinish;
import com.example.graduationprogram.enity.GameItemShow;
import com.example.graduationprogram.sliding_menu.SlideMenu;
import com.example.graduationprogram.util.ToastUtil;

import java.util.ArrayList;
import java.util.List;

public class TabHomeActivity extends AppCompatActivity implements View.OnClickListener {

    private AlertDialog.Builder builder;
    private AlertDialog alertDialog;
    private SlideMenu slideMenu;
    private DatabaseHelper mSQLite;
    private List<String> tableNames;
    private ArrayList<String> tableNamesGame;
    private ArrayList<ArrayList<GameItemShow>> allGame;
    private ListView game_show;
    private ArrayList<GameIsFinish> game_is_finish;
    private int finished;
    private GameShowAdapter gameShowAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_home);

        mSQLite = new DatabaseHelper(this);

        game_show = findViewById(R.id.game_show);
        findViewById(R.id.btn_game_refresh).setOnClickListener(this);
        findViewById(R.id.btn_add_game).setOnClickListener(this);
        findViewById(R.id.btn_member).setOnClickListener(this);
        findViewById(R.id.btn_rank).setOnClickListener(this);
        slideMenu = findViewById(R.id.slideMenu);

        Toolbar tl_more = findViewById(R.id.tl_more);
        tl_more.setNavigationOnClickListener(view -> slideMenu.switchMenu());

        initGetGameShow();

        gameShowAdapter = new GameShowAdapter(TabHomeActivity.this, allGame, game_is_finish, new GameShowAdapter.OnItemClickListener() {
            @Override
            public void onClick(int pos) {
                Intent intent = new Intent(TabHomeActivity.this, GameDetailsShowActivity.class);
                Bundle bundle = new Bundle();
                bundle.putStringArrayList("game_name", tableNamesGame);
                bundle.putInt("game_number", pos);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        }, new GameShowAdapter.OnItemLongClickListener() {
            @Override
            public void onLongClick(int pos) {
                //创建对话框的建造器
                AlertDialog.Builder builder = new AlertDialog.Builder(TabHomeActivity.this);
                //对话框设置内容文本
                builder.setMessage("您确定要删除比赛'"+allGame.get(pos).get(0).getGame_name().split("_",3)[2]+"'吗？");
                //设置对话框的肯定按钮文本及其点击监听器
                builder.setPositiveButton("确定", (dialogInterface, i) -> {
                    mSQLite.delete_game(allGame.get(pos).get(0).getGame_name());
                    initGetGameShow();
                    gameShowAdapter.setAllGame(allGame);
                    gameShowAdapter.setGame_is_finish(game_is_finish);
                    gameShowAdapter.notifyDataSetChanged();
                    ToastUtil.show(TabHomeActivity.this,"删除成功");
                });
                //设置对话框的否定按钮文本及其点击监听器
                builder.setNegativeButton("取消", (dialogInterface, i) -> {
                    ToastUtil.show(TabHomeActivity.this,"取消删除");
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
        game_show.setAdapter(gameShowAdapter);

    }

    @SuppressLint("Range")
    private void initGetGameShow() {
        //查询比赛名称（以game_前缀）
        Cursor cData = mSQLite.GetTableName();
        if (cData != null) {
            int numOfRows = cData.getCount();
            if (numOfRows == 0) {
                Toast.makeText(this, "Game list is empty!", Toast.LENGTH_SHORT).show();
            } else {
                tableNames = new ArrayList<String>();
                cData.moveToFirst();
                while (!cData.isAfterLast()) {
                    tableNames.add(cData.getString(cData.getColumnIndex("name")));
                    cData.moveToNext();
                }

            }
        }
        //提取以game_为前缀的表名称
        tableNamesGame = new ArrayList<>();
        for (int i = 0; i < tableNames.size(); i++) {
            if (tableNames.get(i).split("_", 2)[0].equals("game")) {
                tableNamesGame.add(tableNames.get(i));
            }
        }
       /* for (int i = 0; i < tableNamesGame.size(); i++) {
            Log.d("jin",tableNamesGame.get(i));
        }*/

        game_is_finish = new ArrayList<>();//总场数，已完成，未完成，完成情况
        allGame = new ArrayList<>();
        for (int i = 0; i < tableNamesGame.size(); i++) {
            //取出比赛个体
            ArrayList<GameItemShow> gameItemShows = mSQLite.getGameData(tableNamesGame.get(i));
            Log.d("jin", "比赛名称：" + gameItemShows.get(i).getGame_name() + " 参赛人数：" + gameItemShows.get(i).getPlayer_number());
            allGame.add(gameItemShows);
            //比赛完成场数
            finished = 0;
            //完成情况
            ArrayList<Integer> finish_0_1 = new ArrayList<>();
            for (int j = 0; j < gameItemShows.size(); j++) {
                int is_finish = gameItemShows.get(j).getIs_finish();
                finish_0_1.add(is_finish);
                if (is_finish == 1) {
                    finished += 1;
                }
            }
            //添加到实体类列表中
            game_is_finish.add(new GameIsFinish(gameItemShows.size(), finished, gameItemShows.size() - finished, finish_0_1));//总场数
        }
        //Log.d("jin",""+allGame.get(6).get(0).getPlayer_1());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_add_game:
                //创建对话框建造器
                builder = new AlertDialog.Builder(this);
                LayoutInflater inflater = getLayoutInflater();
                View add_game = inflater.inflate(R.layout.add_game, null);
                //自定义对话框布局
                builder.setView(add_game);
                //为对话框的按钮设置监听
                add_game.findViewById(R.id.btn_rotate).setOnClickListener(this);
                add_game.findViewById(R.id.btn_immobilization).setOnClickListener(this);
                add_game.findViewById(R.id.btn_cancel).setOnClickListener(this);

                //创建对话框
                alertDialog = builder.create();
                //设置对话框可取消
                alertDialog.setCancelable(true);
                //设置对话框显示位置为界面底部
                Window window = alertDialog.getWindow();
                window.setGravity(Gravity.BOTTOM);
                alertDialog.show();
                break;
            case R.id.btn_rotate:
                alertDialog.dismiss();
                //ToastUtil.show(this, "你选择的是轮转搭档循环");
                Intent intent_rotate = new Intent(this, PlayerSelectActivity.class);
                startActivity(intent_rotate);
                break;
            case R.id.btn_immobilization:
                alertDialog.dismiss();
                //ToastUtil.show(this, "你选择的是固定搭档循环");
                break;
            case R.id.btn_cancel:
                alertDialog.dismiss();
                break;
            case R.id.btn_member:
                Intent intent = new Intent();
                intent.setClass(this, MemberActivity.class);
                /* intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);*/
                startActivity(intent);
                break;
            case R.id.btn_game_refresh:
                game_is_finish = new ArrayList<>();//总场数，已完成，未完成，完成情况
                for (int i = 0; i < tableNamesGame.size(); i++) {
                    //取出比赛个体
                    ArrayList<GameItemShow> gameItemShows = mSQLite.getGameData(tableNamesGame.get(i));
                    //比赛完成场数
                    finished = 0;
                    //完成情况
                    ArrayList<Integer> finish_0_1 = new ArrayList<>();
                    for (int j = 0; j < gameItemShows.size(); j++) {
                        int is_finish = gameItemShows.get(j).getIs_finish();
                        finish_0_1.add(is_finish);
                        if (is_finish == 1) {
                            finished += 1;
                        }
                    }
                    //添加到实体类列表中
                    game_is_finish.add(new GameIsFinish(gameItemShows.size(), finished, gameItemShows.size() - finished, finish_0_1));//总场数
                }
                gameShowAdapter.setGame_is_finish(game_is_finish);
                gameShowAdapter.notifyDataSetChanged();
                break;
            case R.id.btn_rank:
                Intent intent1 = new Intent(TabHomeActivity.this, RankShowActivity.class);
                startActivity(intent1);
                break;
        }
    }
}