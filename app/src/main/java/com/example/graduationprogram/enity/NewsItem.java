package com.example.graduationprogram.enity;

public class NewsItem {
    private String title;
    private String path;
    private String desc;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public NewsItem(String title, String path) {
        this.title = title;
        this.path = path;
    }
}
