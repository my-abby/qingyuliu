package com.example.graduationprogram.enity;

public class GameItem {

    private String[] playerItem_1 = new String[2];
    private String[] playerItem_2 = new String[2];
    private String vs;

    public String getVs() {
        return vs;
    }

    public void setVs(String vs) {
        this.vs = vs;
    }

    public GameItem(String[] playerItem_1,String vs, String[] playerItem_2) {
        this.playerItem_1 = playerItem_1;
        this.vs = vs;
        this.playerItem_2 = playerItem_2;
    }

    public String[] getPlayerItem_1() {
        return playerItem_1;
    }

    public void setPlayerItem_1(String[] playerItem_1) {
        this.playerItem_1 = playerItem_1;
    }

    public String[] getPlayerItem_2() {
        return playerItem_2;
    }

    public void setPlayerItem_2(String[] playerItem_2) {
        this.playerItem_2 = playerItem_2;
    }
}
