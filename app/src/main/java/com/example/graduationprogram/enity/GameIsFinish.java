package com.example.graduationprogram.enity;

import java.util.ArrayList;

public class GameIsFinish {
    private int all_number;
    private int finished_number;

    public int getAll_number() {
        return all_number;
    }

    public void setAll_number(int all_number) {
        this.all_number = all_number;
    }

    public int getFinished_number() {
        return finished_number;
    }

    public void setFinished_number(int finished_number) {
        this.finished_number = finished_number;
    }

    public int getTo_finish_number() {
        return to_finish_number;
    }

    public void setTo_finish_number(int to_finish_number) {
        this.to_finish_number = to_finish_number;
    }

    public ArrayList<Integer> getFinish_0_1() {
        return finish_0_1;
    }

    public void setFinish_0_1(ArrayList<Integer> finish_0_1) {
        this.finish_0_1 = finish_0_1;
    }

    public GameIsFinish(int all_number, int finished_number, int to_finish_number, ArrayList<Integer> finish_0_1) {
        this.all_number = all_number;
        this.finished_number = finished_number;
        this.to_finish_number = to_finish_number;
        this.finish_0_1 = finish_0_1;
    }

    private int to_finish_number;
    private ArrayList<Integer> finish_0_1;
}
