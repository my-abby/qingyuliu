package com.example.graduationprogram.enity;

public class TeamItem {

    private String[] team_item = new String[2];

    public TeamItem(String player_1,String player_2) {
        String[] team_item = new String[2];
        team_item[0] = player_1;
        team_item[1] = player_2;
        this.team_item = team_item;
    }

    public void setTeam_item(String[] team_item) {
        this.team_item = team_item;
    }

    public String[] getTeam_item() {
        return team_item;
    }



}
