package com.example.graduationprogram.enity;

public class SingleRanks {
    private int rank;
    private String name_country;
    private String pic_country;
    private String pic_player;
    private String name_player_ch;
    private String name_player_en;
    private String rank_change;
    private String rank_up_or_down;
    private int score;


    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getName_country() {
        return name_country;
    }

    public void setName_country(String name_country) {
        this.name_country = name_country;
    }

    public String getPic_country() {
        return pic_country;
    }

    public void setPic_country(String pic_country) {
        this.pic_country = pic_country;
    }

    public String getPic_player() {
        return pic_player;
    }

    public void setPic_player(String pic_player) {
        this.pic_player = pic_player;
    }

    public String getName_player_ch() {
        return name_player_ch;
    }

    public void setName_player_ch(String name_player_ch) {
        this.name_player_ch = name_player_ch;
    }

    public String getName_player_en() {
        return name_player_en;
    }

    public void setName_player_en(String name_player_en) {
        this.name_player_en = name_player_en;
    }

    public String getRank_change() {
        return rank_change;
    }

    public void setRank_change(String rank_change) {
        this.rank_change = rank_change;
    }

    public String getRank_up_or_down() {
        return rank_up_or_down;
    }

    public void setRank_up_or_down(String rank_up_or_down) {
        this.rank_up_or_down = rank_up_or_down;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
