package com.example.graduationprogram.enity;

public class GameItemShow {
    public String getGame_name() {
        return game_name;
    }

    public void setGame_name(String game_name) {
        this.game_name = game_name;
    }

    private String game_name;
    private String player_1;
    private String player_2;
    private String player_4;
    private int is_finish;
    private int score_1;
    private int score_2;

    public int getPlayer_number() {
        return player_number;
    }

    public void setPlayer_number(int player_number) {
        this.player_number = player_number;
    }

    private int player_number;

    public String getPlayer_1() {
        return player_1;
    }

    public void setPlayer_1(String player_1) {
        this.player_1 = player_1;
    }

    public String getPlayer_2() {
        return player_2;
    }

    public void setPlayer_2(String player_2) {
        this.player_2 = player_2;
    }

    public String getPlayer_3() {
        return player_3;
    }

    public void setPlayer_3(String player_3) {
        this.player_3 = player_3;
    }

    public String getPlayer_4() {
        return player_4;
    }

    public void setPlayer_4(String player_4) {
        this.player_4 = player_4;
    }

    public int getIs_finish() {
        return is_finish;
    }

    public void setIs_finish(int is_finish) {
        this.is_finish = is_finish;
    }

    public int getScore_1() {
        return score_1;
    }

    public void setScore_1(int score_1) {
        this.score_1 = score_1;
    }

    public int getScore_2() {
        return score_2;
    }

    public void setScore_2(int score_2) {
        this.score_2 = score_2;
    }

    private String player_3;

    public GameItemShow(String game_name,int player_number,String player_1, String player_2, String player_3, String player_4, int is_finish, int score_1, int score_2) {
        this.game_name = game_name;
        this.player_number = player_number;
        this.player_1 = player_1;
        this.player_2 = player_2;
        this.player_3 = player_3;
        this.player_4 = player_4;
        this.is_finish = is_finish;
        this.score_1 = score_1;
        this.score_2 = score_2;
    }


}
