package com.example.graduationprogram.enity;

public class DoubleRanks {
    private int rank;
    private String name_country;
    private String pic_country;
    private String pic_player_1;
    private String pic_player_2;
    private String name_player_ch_1;
    private String name_player_ch_2;
    private String name_player_en_1;
    private String name_player_en_2;
    private String rank_change;
    private String rank_up_or_down;
    private int score;

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getName_country() {
        return name_country;
    }

    public void setName_country(String name_country) {
        this.name_country = name_country;
    }

    public String getPic_country() {
        return pic_country;
    }

    public void setPic_country(String pic_country) {
        this.pic_country = pic_country;
    }

    public String getPic_player_1() {
        return pic_player_1;
    }

    public void setPic_player_1(String pic_player_1) {
        this.pic_player_1 = pic_player_1;
    }

    public String getPic_player_2() {
        return pic_player_2;
    }

    public void setPic_player_2(String pic_player_2) {
        this.pic_player_2 = pic_player_2;
    }

    public String getName_player_ch_1() {
        return name_player_ch_1;
    }

    public void setName_player_ch_1(String name_player_ch_1) {
        this.name_player_ch_1 = name_player_ch_1;
    }

    public String getName_player_ch_2() {
        return name_player_ch_2;
    }

    public void setName_player_ch_2(String name_player_ch_2) {
        this.name_player_ch_2 = name_player_ch_2;
    }

    public String getName_player_en_1() {
        return name_player_en_1;
    }

    public void setName_player_en_1(String name_player_en_1) {
        this.name_player_en_1 = name_player_en_1;
    }

    public String getName_player_en_2() {
        return name_player_en_2;
    }

    public void setName_player_en_2(String name_player_en_2) {
        this.name_player_en_2 = name_player_en_2;
    }

    public String getRank_change() {
        return rank_change;
    }

    public void setRank_change(String rank_change) {
        this.rank_change = rank_change;
    }

    public String getRank_up_or_down() {
        return rank_up_or_down;
    }

    public void setRank_up_or_down(String rank_up_or_down) {
        this.rank_up_or_down = rank_up_or_down;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }



}
