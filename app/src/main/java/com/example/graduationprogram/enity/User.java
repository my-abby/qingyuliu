package com.example.graduationprogram.enity;

public class User {
    private int id;
    private String name;
    private String password;
    private int sex;
    private int points;
    private int game_won_number;
    private int game_number;

    public String get_Desc() {
        return _desc;
    }

    public void set_Desc(String desc) {
        this._desc = desc;
    }

    private String _desc;

    public User(String name, String password, int sex,int points,int game_won_number,int game_number,String _desc) {
        super();
        this.name = name;
        this.password = password;
        this.sex = sex;
        this.points = points;
        this.game_won_number = game_won_number;
        this.game_number = game_number;
        this._desc = _desc;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getGame_won_number() {
        return game_won_number;
    }

    public void setGame_won_number(int game_won_number) {
        this.game_won_number = game_won_number;
    }

    public int getGame_number() {
        return game_number;
    }

    public void setGame_number(int game_number) {
        this.game_number = game_number;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "User{id =" + id + ", name = " + name + ",password =" + password + ",sex = " + sex + "}";
    }
}
