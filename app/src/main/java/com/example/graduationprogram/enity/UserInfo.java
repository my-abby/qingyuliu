package com.example.graduationprogram.enity;

import java.util.ArrayList;

public class UserInfo {
    public int sex;//性别
    public String name;//姓名

    public boolean bPressed;
    public int id;
    private static int seq = 0;

    public UserInfo(int sex, String name) {
        this.sex = sex;
        this.name = name;
        this.bPressed = false;
        this.id = this.seq;
        this.seq++;
    }

    public static ArrayList<UserInfo> getDefaultList(ArrayList<Integer> listSexArray,ArrayList<String> listNameArray) {
        ArrayList<UserInfo> listArray = new ArrayList<UserInfo>();
        for (int i=0; i<listNameArray.size(); i++) {
            listArray.add(new UserInfo(listSexArray.get(i), listNameArray.get(i)));
        }
        return listArray;
    }}