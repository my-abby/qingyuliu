package com.example.graduationprogram.enity;

import java.util.ArrayList;

public class NewsContent {
    private ArrayList<String> news_content;

    public ArrayList<String> getNews_content() {
        return news_content;
    }

    public void setNews_content(String news_content) {
        this.news_content.add(news_content);
    }
}
