package com.example.graduationprogram.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import com.example.graduationprogram.enity.GameItemShow;
import com.example.graduationprogram.enity.User;
import com.example.graduationprogram.util.ToastUtil;

import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {
    //创建一个数据库
    private SQLiteDatabase db;

    public DatabaseHelper(@Nullable Context context) {
        super(context, "db_test", null, 1);
        db = getReadableDatabase();
    }

    public DatabaseHelper(Context context, int version) {
        super(context, "db_test", null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //在第一次创建数据库的时候，创建一些字段
        String sql = "create table user(name varchar(50), password varchar(40),sex integer,points integer,game_won_number integer,game_number integer,_desc varchar(100))";
        db.execSQL(sql);//sql语句的执行函数
    }

    //动态创建表，若存在则先删除再创建
    public void createNewTable(String sql, String table_name) {
        db.execSQL("DROP TABLE IF EXISTS " + table_name);
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //如果这个表中存在user,我们可以先把他去掉,然后重新创建
        String sql = "DROP TABLE IF EXISTS user";
        db.execSQL(sql);
        onCreate(db);
    }

    public void refresh_points(String user_name, int points, int game_won_number, int game_number) {
        //Log.d("jin",user_name);
        String sql_1 = "update user set points="+points+" where name='"+user_name+"'";
        String sql_2 = "update user set game_won_number=" + game_won_number + " where name='"+user_name+"'";
        String sql_3 = "update user set game_number=" + game_number + " where name='"+user_name+"'";
        db.execSQL(sql_1);
        db.execSQL(sql_2);
        db.execSQL(sql_3);
    }

    public void modify_password(String user_name,String new_password){
        String sql = "update user set password="+new_password+" where name='"+user_name+"'";
        db.execSQL(sql);
    }

    public int[] query_points(String user_name) {
        int[] point_info = new int[3];
        Cursor cursor = db.rawQuery("select * from user where name=?", new String[]{user_name});
        if (cursor.getCount() > 0) {
            cursor.moveToNext();
            int index_points = cursor.getColumnIndex("points");
            int index_game_won_number = cursor.getColumnIndex("game_won_number");
            int index_game_number = cursor.getColumnIndex("game_number");
            int points = cursor.getInt(index_points);
            int game_won_number = cursor.getInt(index_game_won_number);
            int game_number = cursor.getInt(index_game_number);
            point_info = new int[]{points, game_won_number, game_number};
        }
        return point_info;
    }

    //为使项目结构更加紧凑，我们在此类中编写增删改查的函数,因为只有登录和注册界面,因此只涉及到写入数据库insert和query的操作
    public void insert(String name, String password, int sex) {
        db.execSQL("insert into user(name,password,sex,points,game_won_number,game_number,_desc)VALUES(?,?,?,?,?,?,?)", new Object[]{name, password, sex,0,0,0,"这个人很懒，还没有写描述！"});
    }

    public ArrayList<User> getAllDATA() {//查询数据库
        ArrayList<User> list = new ArrayList<User>();
        //查询数据库中的数据,并将这些数据按照降序的情况排列
        Cursor cursor = db.query("user", null, null, null, null, null, "points DESC");
        while (cursor.moveToNext()) {
            int index_name = cursor.getColumnIndex("name");
            int index_password = cursor.getColumnIndex("password");
            int index_sex = cursor.getColumnIndex("sex");
            int index_points = cursor.getColumnIndex("points");
            int index_game_won_number = cursor.getColumnIndex("game_won_number");
            int index_game_number = cursor.getColumnIndex("game_number");
            int index_desc = cursor.getColumnIndex("_desc");
            String name = cursor.getString(index_name);
            String password = cursor.getString(index_password);
            int sex = cursor.getInt(index_sex);
            int points = cursor.getInt(index_points);
            int game_won_number = cursor.getInt(index_game_won_number);
            int game_number = cursor.getInt(index_game_number);
            String _desc = cursor.getString(index_desc);

            list.add(new User(name, password,sex,points,game_won_number,game_number,_desc));
        }
        return list;
    }

    public ArrayList<GameItemShow> getGameData(String table_name) {//查询数据库
        ArrayList<GameItemShow> game_list = new ArrayList<GameItemShow>();
        int player_number = Integer.valueOf(table_name.split("_", 3)[1]);
        //查询数据库中比赛的数据
        Cursor cursor = db.query(table_name, null, null, null, null, null, null);
        while (cursor.moveToNext()) {
            int index_player_1 = cursor.getColumnIndex("player_1");
            int index_player_2 = cursor.getColumnIndex("player_2");
            int index_player_3 = cursor.getColumnIndex("player_3");
            int index_player_4 = cursor.getColumnIndex("player_4");
            int index_is_finish = cursor.getColumnIndex("is_finish");
            int index_score_1 = cursor.getColumnIndex("score_1");
            int index_score_2 = cursor.getColumnIndex("score_2");
            String player_1 = cursor.getString(index_player_1);
            String player_2 = cursor.getString(index_player_2);
            String player_3 = cursor.getString(index_player_3);
            String player_4 = cursor.getString(index_player_4);
            int is_finish = cursor.getInt(index_is_finish);
            int score_1 = cursor.getInt(index_score_1);
            int score_2 = cursor.getInt(index_score_2);
            game_list.add(new GameItemShow(table_name, player_number, player_1, player_2, player_3, player_4, is_finish, score_1, score_2));
        }
        return game_list;
    }

    //插入比赛
    public void insert_game(String table_name, int _id, String player_1, String player_2, String player_3, String player_4, int is_finish, int score_1, int score_2) {
        db.execSQL("insert into " + table_name + "(_id,player_1,player_2,player_3,player_4,is_finish,score_1,score_2)VALUES(?,?,?,?,?,?,?,?)", new Object[]{_id, player_1, player_2, player_3, player_4, is_finish, score_1, score_2});
    }

    //获取比赛表的名称
    public Cursor GetTableName() {
        SQLiteDatabase sdb = this.getReadableDatabase();
        Cursor cursor = sdb.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
        return cursor;
    }

    //更新比赛比分
    public void setGamePoints(String table_name, int pos, int point_1, int point_2) {
        String sql_1 = "update " + table_name + " set is_finish=1" + " where _id=" + pos;
        String sql_2 = "update " + table_name + " set score_1=" + point_1 + " where _id=" + pos;
        String sql_3 = "update " + table_name + " set score_2=" + point_2 + " where _id=" + pos;
        db.execSQL(sql_1);
        db.execSQL(sql_2);
        db.execSQL(sql_3);
    }
    //删除表（比赛）
    public void delete_game(String game_name){
        String sql = "DROP TABLE IF EXISTS "+game_name;
        db.execSQL(sql);
    }
}
