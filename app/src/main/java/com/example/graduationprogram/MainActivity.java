package com.example.graduationprogram;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.graduationprogram.adapter.DoubleAdapter;
import com.example.graduationprogram.enity.DoubleRanks;
import com.example.graduationprogram.enity.SingleRanks;
import com.example.graduationprogram.task.DoubleTask;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView Lv_main_list;
    private List<DoubleRanks> RanksList = new ArrayList<DoubleRanks>();
    private ProgressDialog progressDialog;
    private DoubleAdapter myAdapter;
    private SwipeRefreshLayout SwipeRefresh;
    //String path = "https://www.badmintoncn.com/ranking.php?rw=&type=6";
    //String path = "https://www.badmintoncn.com/ranking.php?rw=&type=7";
    String path = "https://www.badmintoncn.com/ranking.php?rw=&type=8";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Lv_main_list = (ListView) findViewById(R.id.Lv_main_list);
        //实例化进度条对话框
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("亲，正在玩命加载中哦！");
        //实例化适配器
        myAdapter = new DoubleAdapter(RanksList, MainActivity.this);
        Lv_main_list.setAdapter(myAdapter);
    }

    //获取xml数据
    public void getJson(View view) {
        DoubleTask myTask = new DoubleTask(RanksList, myAdapter, path);
        myTask.execute();
    }
}