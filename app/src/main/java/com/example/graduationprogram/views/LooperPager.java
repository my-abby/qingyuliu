package com.example.graduationprogram.views;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.graduationprogram.R;
import com.example.graduationprogram.util.SizeUtils;

public class LooperPager extends LinearLayout {

    private ViewPager view_pager;
    private LinearLayout point_container;
    private TextView tv_title;
    private BindTitleListener mTitleSetListener = null;
    private InnerAdapter mInnerAdapter = null;


    public LooperPager(Context context) {
        this(context, null);
    }

    public LooperPager(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LooperPager(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.layout_looper_pager, this, true);
        init();
    }

    private void init() {
        initView();
        initEvent();
    }

    private void initEvent() {
        view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                //切换的一个回调方法

            }

            @Override
            public void onPageSelected(int position) {
                //切换停下来的回调
                if (mInnerAdapter != null) {
                    //停下来以后，设置标题
                    int realPosition = position % mInnerAdapter.getDataSize();
                    //
                    if (mTitleSetListener != null) {
                        tv_title.setText(mTitleSetListener.getTitle(realPosition));
                        tv_title.setTextColor(Color.BLACK);
                    }
                    //切换指示器焦点
                    updateIndicator();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                //切换状态改变的回调

            }
        });
    }

    public interface BindTitleListener {
        String getTitle(int position);
    }

    public void setData(InnerAdapter innerAdapter, BindTitleListener listener) {
        this.mTitleSetListener = listener;
        view_pager.setAdapter(innerAdapter);
        view_pager.setCurrentItem(Integer.MAX_VALUE / 2);
        this.mInnerAdapter = innerAdapter;

        if (listener != null) {
            tv_title.setText(listener.getTitle(view_pager.getCurrentItem() % mInnerAdapter.getDataSize()));
        }
        //可以得到数据的个数，根据数据个数，动态创建原点，indicator
        updateIndicator();
    }

    public abstract static class InnerAdapter extends PagerAdapter {

        private OnPagerItemClickListener mItemClickListener = null;

        @Override
        public int getCount() {
            return Integer.MAX_VALUE;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            final int realPosition = position % getDataSize();
            View itemView = getSubView(container, realPosition);
            itemView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mItemClickListener != null) {
                        mItemClickListener.onItemClick(realPosition);
                    }
                }
            });
            container.addView(itemView);
            return itemView;
        }

        protected abstract int getDataSize();

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }

        protected abstract View getSubView(ViewGroup container, int position);

        public void setPagerItemClickListener(OnPagerItemClickListener itemClickListener){
            this.mItemClickListener = itemClickListener;
        }
        public interface OnPagerItemClickListener{
            void onItemClick(int position);
        }
    }



    private void updateIndicator() {
        if (mInnerAdapter != null && mTitleSetListener != null) {
            int count = mInnerAdapter.getDataSize();
            point_container.removeAllViews();
            for (int i = 0; i < count; i++) {
                View point = new View(getContext());
                if (view_pager.getCurrentItem() % mInnerAdapter.getDataSize() == i) {
                    //point.setBackgroundColor(Color.parseColor("#ff0000"));
                    point.setBackground(getResources().getDrawable(R.drawable.shape_circle_red));
                } else {
                    //point.setBackgroundColor(Color.parseColor("#ffffff"));
                    point.setBackground(getResources().getDrawable(R.drawable.shape_circle_white));
                }

                //设置大小
                LinearLayout.LayoutParams layoutParams = new LayoutParams(SizeUtils.dip2px(getContext(), 8), SizeUtils.dip2px(getContext(), 8));
                layoutParams.setMargins(SizeUtils.dip2px(getContext(), 8), 0, SizeUtils.dip2px(getContext(), 8), 0);
                point.setLayoutParams(layoutParams);
                //添加到容器里面去
                point_container.addView(point);
            }
        }
    }

    private void initView() {
        view_pager = findViewById(R.id.vp_looper_pager);
        view_pager.setOffscreenPageLimit(3);
        view_pager.setPageMargin(SizeUtils.dip2px(getContext(),10));
        point_container = findViewById(R.id.looper_point_container_iv);
        tv_title = findViewById(R.id.tv_looper_title);
    }
}
