package com.example.graduationprogram.util;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.graduationprogram.NewsContentActivity;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

public class NewsData {
    public static void DownNewsData(Context context, String path, ArrayList<String> news_content,int position){
        //创建OkHttpClient
        OkHttpClient client = new OkHttpClient();

        //创建请求
        Request request = new Request.Builder()
                .url(path)
                .build();

        //发送请求并处理响应
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {

                String html = response.body().string();
                Document doc = Jsoup.parse(html);
                //标题
                String element_h = doc.getElementsByTag("h2").text();
                news_content.add(element_h);
                switch (position){
                    case 0:
                        //内容
                        Elements element_p0 = doc.getElementsByAttributeValue("style", "text-indent:2em;");
                        news_content.add(element_p0.get(0).text());
                        //图片
                        String element_pic0 = doc.getElementsByAttributeValue("style", "height:auto;").attr("src");
                        news_content.add(element_pic0);
                        //内容
                        for (int i = 1; i < element_p0.size(); i++) {
                            news_content.add(element_p0.get(i).text());
                        }
                        break;
                    case 1:
                        //内容
                        Elements element_p1 = doc.getElementsByAttributeValue("style", "text-indent:2em;");
                        news_content.add(element_p1.get(0).text());
                        //图片
                        String element_pic1 = doc.getElementsByAttributeValue("alt", "何济霆/周昊东").attr("src");
                        news_content.add(element_pic1);
                        //内容
                        for (int i = 1; i < element_p1.size(); i++) {
                            news_content.add(element_p1.get(i).text());
                        }
                        break;
                    case 2:
                        //图片
                        String element_pic2 = doc.getElementsByAttributeValue("alt", "李诗沣").attr("src");
                        news_content.add(element_pic2);
                        //内容
                        Elements element_p2 = doc.getElementsByAttributeValue("style", "text-indent:2em;");
                        news_content.add(element_p2.get(0).text());
                        //图片
                        String element_pic2_1 = doc.getElementsByAttributeValue("alt", "石宇奇").attr("src");
                        news_content.add(element_pic2_1);
                        //内容
                        news_content.add(element_p2.get(1).text());
                        //图片
                        String element_pic2_2 = doc.getElementsByAttributeValue("alt", "陈雨菲").attr("src");
                        news_content.add(element_pic2_2);
                        //内容
                        news_content.add(element_p2.get(2).text());
                        //图片
                        String element_pic2_3 = doc.getElementsByAttributeValue("alt", "郑思维/黄雅琼").attr("src");
                        news_content.add(element_pic2_3);
                        //内容
                        news_content.add(element_p2.get(3).text());
                        //图片
                        String element_pic2_4 = doc.getElementsByAttributeValue("alt", " ").attr("src");
                        news_content.add(element_pic2_4);
                        //内容
                        news_content.add(element_p2.get(4).text());
                        //图片
                        String element_pic2_5 = doc.getElementsByAttributeValue("alt", "金昭映/孔熙容").attr("src");
                        String[] element_pic2_5_ = element_pic2_5.split(":",2);
                        news_content.add("https:"+element_pic2_5_[1]);
                        //内容
                        news_content.add(element_p2.get(5).text());
                        break;
                    case 3:
                        //内容
                        Elements element_p3 = doc.getElementsByAttributeValue("style", "text-indent:2em;");
                        news_content.add(element_p3.get(0).text());
                        //图片
                        String element_pic3 = doc.getElementsByAttributeValue("alt", "冯彦哲/黄东萍").attr("src");
                        news_content.add(element_pic3);
                        //内容
                        for (int i = 1; i < 4; i++) {
                            news_content.add(element_p3.get(i).text());
                        }
                        //图片
                        String element_pic3_1 = doc.getElementsByAttributeValue("alt", "李诗沣不敌伍家朗").attr("src");
                        news_content.add(element_pic3_1);
                        //内容
                        for (int i = 4; i < 6; i++) {
                            news_content.add(element_p3.get(i).text());
                        }
                        //图片
                        String element_pic3_2 = doc.getElementsByAttributeValue("alt", "山口茜").attr("src");
                        news_content.add(element_pic3_2);
                        //内容
                        news_content.add(element_p3.get(6).text());
                        break;
                }

                //Log.d("jin","1"+news_content.get(0));
                Bundle bundle = new Bundle();
                bundle.putStringArrayList("news_content", news_content);
                Intent intent = new Intent();
                intent.putExtras(bundle);
                intent.setClass(context, NewsContentActivity.class);
                context.startActivity(intent);
            }
        });

    }
}
