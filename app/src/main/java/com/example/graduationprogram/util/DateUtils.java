package com.example.graduationprogram.util;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
    public static String GetNowTime(){
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(new Date());
    }
}
