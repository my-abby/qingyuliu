package com.example.graduationprogram.util;

import android.util.Log;

import com.example.graduationprogram.R;
import com.example.graduationprogram.enity.User;
import java.util.ArrayList;

public class Member_array {
    //获取数据库中的性别数据，并组成整数数组
    public static ArrayList<Integer> get_sex(ArrayList<User> data) {
        ArrayList<Integer> listSexArray = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            User userdata = data.get(i);
            int sex = userdata.getSex();
            if (sex == 1) {
                listSexArray.add(R.drawable.male);
            } else {
                listSexArray.add(R.drawable.female);
            }
        }
        return listSexArray;
    }
    //获取数据库中的姓名数据，并组成字符串数组
    public static ArrayList<String> get_name(ArrayList<User> data) {
        ArrayList<String> listNameArray = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            User userdata = data.get(i);
            String name = userdata.getName();
            listNameArray.add(name);
        }
        return listNameArray;
    }
}
