package com.example.graduationprogram.util;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class GlideUtils {
    private GlideUtils(){}

    private  static GlideUtils mGlideUtils;

    public static GlideUtils getInstance(){
        if (mGlideUtils == null) {
            synchronized (GlideUtils.class){
                if (mGlideUtils == null) {
                    mGlideUtils = new GlideUtils();
                }
            }
        }
        return mGlideUtils;
    }

    public void ImageLoader(Context context, String path, ImageView imageView,int width,int height){
        Glide.with(context)
                .load(path)
                .override(width,height)
                .into(imageView);
    }

    public void GifLoader(Context context,String path,ImageView imageView){
        Glide.with(context)
                .load(path)
                .into(imageView);
    }

}
