package com.example.graduationprogram;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.example.graduationprogram.adapter.RankShowAdapter;
import com.example.graduationprogram.database.DatabaseHelper;
import com.example.graduationprogram.enity.User;

import java.util.ArrayList;

public class RankShowActivity extends AppCompatActivity {

    private DatabaseHelper mSQLite;
    private ArrayList<User> data;
    private ListView rank_show;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rank_show);

        rank_show = findViewById(R.id.rank_show);
        Toolbar tl_back = findViewById(R.id.tl_back);
        tl_back.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mSQLite = new DatabaseHelper(this);
        data = mSQLite.getAllDATA();
        //Log.d("jin",""+data.get(0).getPoints());

        RankShowAdapter rankShowAdapter = new RankShowAdapter(data,RankShowActivity.this);
        rank_show.setAdapter(rankShowAdapter);

    }
}