package com.example.graduationprogram.fragment;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.graduationprogram.R;
import com.example.graduationprogram.adapter.DoubleAdapter;
import com.example.graduationprogram.enity.DoubleRanks;
import com.example.graduationprogram.task.DoubleTask;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.CheckedOutputStream;

public class DoubleFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private View view;
    private ListView Lv_main_list;
    private List<DoubleRanks> RanksList = new ArrayList<DoubleRanks>();
    private ProgressDialog progressDialog;
    private DoubleAdapter myAdapter;
    private SwipeRefreshLayout SwipeRefresh;
    private String path;

    public DoubleFragment(String path) {
        this.path = path;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.rank_fragment, container, false);
            //实例化下拉刷新
            SwipeRefresh = view.findViewById(R.id.SwipeRefresh);
            SwipeRefresh.setOnRefreshListener(this);
            SwipeRefresh.setColorSchemeColors(Color.BLUE);

            Lv_main_list = (ListView) view.findViewById(R.id.Lv_main_list);

            /*progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("亲，正在玩命加载中哦！");*/
            //实例化适配器
            myAdapter = new DoubleAdapter(RanksList, getContext());
            Lv_main_list.setAdapter(myAdapter);
        }
        return view;
    }

    public void goTask() {
        DoubleTask myTask = new DoubleTask(RanksList, myAdapter, path);
        new Thread() {
            public void run() {
                myTask.execute();
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                SwipeRefresh.setRefreshing(false);
            }
        }.start();

    }

    @Override
    public void onRefresh() {
        goTask();
    }
}

