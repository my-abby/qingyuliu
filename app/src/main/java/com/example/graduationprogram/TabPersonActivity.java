package com.example.graduationprogram;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.graduationprogram.database.DatabaseHelper;
import com.example.graduationprogram.enity.User;
import com.example.graduationprogram.util.ToastUtil;

import java.util.ArrayList;

public class TabPersonActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView iv_sex_per;
    private TextView tv_name_per;
    private AlertDialog alertDialog;
    private View per_info;
    private View game_info;
    private View modify_password;
    private DatabaseHelper mSQLite;
    private ArrayList<User> data;
    private String name;
    private AlertDialog.Builder builder;
    private String old_password;
    private String new_password;
    private String new2_password;
    private int sex;
    private EditText et_old_password;
    private EditText et_new_password;
    private EditText et_new2_password;
    private SharedPreferences shared;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_person);

        mSQLite = new DatabaseHelper(this);
        data = mSQLite.getAllDATA();

        iv_sex_per = findViewById(R.id.iv_sex_per);
        tv_name_per = findViewById(R.id.tv_name_per);

        findViewById(R.id.btn_per_info).setOnClickListener(this);
        findViewById(R.id.btn_game_info).setOnClickListener(this);
        findViewById(R.id.btn_modify_password).setOnClickListener(this);
        findViewById(R.id.btn_quit).setOnClickListener(this);

        shared = getSharedPreferences("share_login", MODE_PRIVATE);
        name = shared.getString("name", "");
        sex = shared.getInt("sex", 0);
        if (sex == 1) {
            iv_sex_per.setImageResource(R.drawable.male);
        } else {
            iv_sex_per.setImageResource(R.drawable.female);
        }

        tv_name_per.setText(name);
        tv_name_per.setTextSize(30);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_per_info:
                //创建对话框建造器
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                LayoutInflater inflater = getLayoutInflater();
                per_info = inflater.inflate(R.layout.layout_per_info, null);

                TextView tv_name = per_info.findViewById(R.id.tv_name_info);
                TextView tv_sex = per_info.findViewById(R.id.tv_sex_info);
                TextView tv_desc = per_info.findViewById(R.id.tv_desc_info);

                for (int i = 0; i < data.size(); i++) {
                    //Log.d("jin",name);
                    if (data.get(i).getName().equals(name)) {
                        tv_name.setText(name);
                        if (sex == 1) {
                            tv_sex.setText("男");
                        } else {
                            tv_sex.setText("女");
                        }
                        tv_desc.setText(data.get(i).get_Desc());
                    }
                }
                //自定义对话框布局
                builder.setView(per_info);
                //创建对话框
                alertDialog = builder.create();
                //设置对话框可取消
                alertDialog.setCancelable(true);
                //设置对话框显示位置为界面中间
                Window window = alertDialog.getWindow();
                window.setGravity(Gravity.CENTER);
                alertDialog.show();
                break;
            case R.id.btn_game_info:
                //创建对话框建造器
                AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
                LayoutInflater inflater1 = getLayoutInflater();
                game_info = inflater1.inflate(R.layout.layout_game_info, null);

                TextView tv_points_info = game_info.findViewById(R.id.tv_points_info);
                TextView tv_game_number_info = game_info.findViewById(R.id.tv_game_number_info);
                TextView tv_game_won_number_info = game_info.findViewById(R.id.tv_game_won_number_info);
                TextView tv_game_ratio_info = game_info.findViewById(R.id.tv_game_ratio_info);

                for (int i = 0; i < data.size(); i++) {
                    if (data.get(i).getName().equals(name)) {
                        tv_points_info.setText(data.get(i).getPoints() + "");
                        tv_game_number_info.setText(data.get(i).getGame_number() + "");
                        tv_game_won_number_info.setText(data.get(i).getGame_won_number() + "");
                        String ratio = String.format("%.2f",100*(float)data.get(i).getGame_won_number()/(float) data.get(i).getGame_number());
                        if (!Float.isNaN((float)data.get(i).getGame_won_number()/(float) data.get(i).getGame_number())){
                            tv_game_ratio_info.setText(ratio+"%");
                        }else {
                            tv_game_ratio_info.setText(ratio+"");
                        }
                    }
                }
                //自定义对话框布局
                builder1.setView(game_info);
                //创建对话框
                alertDialog = builder1.create();
                //设置对话框可取消
                alertDialog.setCancelable(true);
                //设置对话框显示位置为界面中间
                Window window1 = alertDialog.getWindow();
                window1.setGravity(Gravity.CENTER);
                alertDialog.show();
                break;
            case R.id.btn_modify_password:
                //创建对话框建造器
                AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                LayoutInflater inflater2 = getLayoutInflater();
                modify_password = inflater2.inflate(R.layout.layout_modify_password, null);

                et_old_password = modify_password.findViewById(R.id.et_old_password);
                et_new_password = modify_password.findViewById(R.id.et_new_password);
                et_new2_password = modify_password.findViewById(R.id.et_new2_password);
                modify_password.findViewById(R.id.btn_confirm_modify).setOnClickListener(this);

                //自定义对话框布局
                builder2.setView(modify_password);
                //创建对话框
                alertDialog = builder2.create();
                //设置对话框可取消
                alertDialog.setCancelable(true);
                //设置对话框显示位置为界面中间
                Window window2 = alertDialog.getWindow();
                window2.setGravity(Gravity.CENTER);
                alertDialog.show();
                break;
            case R.id.btn_confirm_modify:
                old_password = et_old_password.getText().toString();
                new_password = et_new_password.getText().toString();
                new2_password = et_new2_password.getText().toString();

                for (int i = 0; i < data.size(); i++) {
                    if (data.get(i).getName().equals(name)) {
                        if (!old_password.equals("")) {
                            if (old_password.equals(data.get(i).getPassword())) {
                                if ((!new_password.equals("")) && (!new2_password.equals(""))) {
                                    if (new_password.equals(new2_password)) {
                                        mSQLite.modify_password(name, new_password);
                                        ToastUtil.show(TabPersonActivity.this, "修改成功！");
                                        alertDialog.dismiss();
                                    } else {
                                        ToastUtil.show(TabPersonActivity.this, "两次输入的新密码不相同！");
                                    }
                                } else {
                                    ToastUtil.show(TabPersonActivity.this, "请输入或确认新密码！");
                                }
                            } else {
                                ToastUtil.show(TabPersonActivity.this, "旧密码错误，请重新输入！");
                            }
                        } else {
                            ToastUtil.show(TabPersonActivity.this, "请输入旧密码！");
                        }
                    }
                }
                break;
            case R.id.btn_quit:
                Intent intent = new Intent(TabPersonActivity.this,LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
        }
    }
}