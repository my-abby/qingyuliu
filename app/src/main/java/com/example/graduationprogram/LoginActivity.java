package com.example.graduationprogram;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.graduationprogram.database.DatabaseHelper;
import com.example.graduationprogram.util.ToastUtil;
import com.example.graduationprogram.enity.User;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private DatabaseHelper mSQLite;
    private EditText et_name;
    private EditText et_password;
    private SharedPreferences shared;
    private CheckBox is_remember;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        findViewById(R.id.btn_login).setOnClickListener(this);
        findViewById(R.id.btn_reg).setOnClickListener(this);
        et_name = findViewById(R.id.et_name);
        et_password = findViewById(R.id.et_password);
        mSQLite = new DatabaseHelper(this);

        is_remember = findViewById(R.id.is_remember);

        //获取共享参数对象
        shared = getSharedPreferences("share_login",MODE_PRIVATE);
        //获取共享参数保存的用户名
        String name_login = shared.getString("name_login","");
        //获取共享参数保存的密码
        String password_login = shared.getString("password_login","");
        if (!name_login.equals("")){
            is_remember.setChecked(true);
        }
        et_name.setText(name_login);
        et_password.setText(password_login);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                String name = et_name.getText().toString().trim();
                String password = et_password.getText().toString().trim();

                ArrayList<User> data = mSQLite.getAllDATA();
                boolean flag = false;
                int sex = 1;
                for (int i = 0; i < data.size(); i++) {
                    User userdata = data.get(i);
                    if (name.equals(userdata.getName()) && password.equals(userdata.getPassword())) {
                        sex = userdata.getSex();
                        flag = true;
                        break;
                    } else {
                        flag = false;
                    }
                }

                if (!TextUtils.isEmpty(name) && !TextUtils.isEmpty(password)) {
                    if (flag) {
                        shared = getSharedPreferences("share_login", MODE_PRIVATE);
                        SharedPreferences.Editor editor = shared.edit();
                        //记住密码
                        if (is_remember.isChecked()){
                            editor.putString("name_login",name);
                            editor.putString("password_login",password);
                        }else {
                            editor.putString("name_login","");
                            editor.putString("password_login","");
                        }
                        editor.putString("name", name);
                        editor.putInt("sex", sex);
                        editor.commit();
                        //创建一个意图对象，准备跳到指定的活动页面
                        Intent intent1 = new Intent(this, TabGroupActivity.class);
                        //设置启动标志：跳转到新页面时，栈中的原有实例都被清空，同时开辟新任务的活动栈
                        intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        //跳转到意图指定的活动页面
                        startActivity(intent1);
                        finish();
                        ToastUtil.show(this, "登录成功");
                    } else {
                        ToastUtil.show(this, "用户名或密码不正确");
                    }
                } else {
                    ToastUtil.show(this, "用户名与密码不能为空");
                }
                break;
            case R.id.btn_reg:
                Intent intent2 = new Intent(this, RegisterActivity.class);
                startActivity(intent2);
                break;
        }
    }
}