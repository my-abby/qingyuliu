package com.example.graduationprogram;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.graduationprogram.adapter.GameDetailsShowAdapter;
import com.example.graduationprogram.database.DatabaseHelper;
import com.example.graduationprogram.enity.GameItem;
import com.example.graduationprogram.enity.GameItemShow;
import com.example.graduationprogram.util.ToastUtil;

import java.util.ArrayList;

public class GameDetailsShowActivity extends AppCompatActivity {

    private DatabaseHelper mSQLite;
    private ArrayList<GameItem> game_list;
    private ArrayList<GameItemShow> game_item_show;
    private GameDetailsShowAdapter gameDetailsShowAdapter;
    private AlertDialog alertDialog;
    private AlertDialog.Builder builder;
    private EditText et_item_points_1;
    private EditText et_item_points_2;
    private ListView game_details_show;
    private ArrayList<String> game_name;
    private int game_number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_details_show);

        game_details_show = findViewById(R.id.game_details_show);

        Toolbar tl_back = findViewById(R.id.tl_back);
        tl_back.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Bundle bundle = this.getIntent().getExtras();
        game_name = bundle.getStringArrayList("game_name");
        game_number = bundle.getInt("game_number");
        //Log.d("jin",game_name.toString()+game_number);

        mSQLite = new DatabaseHelper(this);
        game_item_show = mSQLite.getGameData(game_name.get(game_number));

        game_list = new ArrayList<>();
        for (int i = 0; i < game_item_show.size(); i++) {
            game_list.add(new GameItem(new String[]{game_item_show.get(i).getPlayer_1(), game_item_show.get(i).getPlayer_2()}, "vs", new String[]{game_item_show.get(i).getPlayer_3(), game_item_show.get(i).getPlayer_4()}));
        }

        gameDetailsShowAdapter = new GameDetailsShowAdapter(game_item_show,game_list, GameDetailsShowActivity.this, new GameDetailsShowAdapter.OnItemClickListener() {
            @Override
            public void onClick(int pos) {
                //创建对话框建造器
                builder = new AlertDialog.Builder(GameDetailsShowActivity.this);
                //自定义对话框布局
                LayoutInflater inflater = getLayoutInflater();
                View dialog_points = inflater.inflate(R.layout.dialog_points, null);
                builder.setView(dialog_points);
                //为对话框的按钮设置监听
                TextView tv_player_item_1 = dialog_points.findViewById(R.id.tv_player_item_1);
                TextView tv_player_item_2 = dialog_points.findViewById(R.id.tv_player_item_2);
                tv_player_item_1.setText(game_list.get(pos).getPlayerItem_1()[0] + "&" + game_list.get(pos).getPlayerItem_1()[1]);
                tv_player_item_2.setText(game_list.get(pos).getPlayerItem_2()[0] + "&" + game_list.get(pos).getPlayerItem_2()[1]);
                dialog_points.findViewById(R.id.point_cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //ToastUtil.show(GameDetailsActivity.this, "1233");
                        alertDialog.dismiss();
                    }
                });
                dialog_points.findViewById(R.id.point_confirm).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        et_item_points_1 = dialog_points.findViewById(R.id.et_item_points_1);
                        et_item_points_2 = dialog_points.findViewById(R.id.et_item_points_2);
                        String item_points_1 = et_item_points_1.getText().toString().trim();
                        String item_points_2 = et_item_points_2.getText().toString().trim();

                        if (!item_points_1.equals("") && !item_points_2.equals("")) {
                            if (Integer.valueOf(item_points_1) > 30 || Integer.valueOf(item_points_2) > 30 ||
                                    Integer.valueOf(item_points_1) == Integer.valueOf(item_points_2)) {
                                ToastUtil.show(GameDetailsShowActivity.this, "请输入正确的比分");
                                return;
                            }
                            game_list.get(pos).setVs(item_points_1 + ":" + item_points_2);
                            game_item_show = mSQLite.getGameData(game_name.get(game_number));
                            gameDetailsShowAdapter.setGame_item_show(game_item_show);
                            gameDetailsShowAdapter.notifyDataSetChanged();
                            alertDialog.dismiss();

                            //将比分计入数据库，并为胜者增加积分
                            mSQLite.setGamePoints(game_name.get(game_number), pos, Integer.valueOf(item_points_1), Integer.valueOf(item_points_2));
                            //更新参与者积分
                            if (Integer.valueOf(item_points_1) > Integer.valueOf(item_points_2)) {
                                //player_1
                                points_refresh(game_list.get(pos).getPlayerItem_1()[0], 1);
                                //player_2
                                points_refresh(game_list.get(pos).getPlayerItem_1()[1], 1);
                                //player_3
                                points_refresh(game_list.get(pos).getPlayerItem_2()[0], 0);
                                //player_4
                                points_refresh(game_list.get(pos).getPlayerItem_2()[1], 0);
                            } else {
                                //player_1
                                points_refresh(game_list.get(pos).getPlayerItem_1()[0], 0);
                                //player_2
                                points_refresh(game_list.get(pos).getPlayerItem_1()[1], 0);
                                //player_3
                                points_refresh(game_list.get(pos).getPlayerItem_2()[0], 1);
                                //player_4
                                points_refresh(game_list.get(pos).getPlayerItem_2()[1], 1);
                            }

                        } else {
                            alertDialog.dismiss();
                        }
                    }
                });
                //创建对话框
                alertDialog = builder.create();
                //设置对话框可取消
                alertDialog.setCancelable(true);
                alertDialog.show();
            }
        });

        game_details_show.setAdapter(gameDetailsShowAdapter);

    }

    private void points_refresh(String player_name, int tag) {
        switch (tag) {
            case 1:
                int[] points_player_win = mSQLite.query_points(player_name);
                points_player_win[0] += 1;
                points_player_win[1] += 1;
                points_player_win[2] += 1;
                mSQLite.refresh_points(player_name, points_player_win[0], points_player_win[1], points_player_win[2]);
                break;
            case 0:
                int[] points_player_lose = mSQLite.query_points(player_name);
                points_player_lose[0] += 0;
                points_player_lose[1] += 0;
                points_player_lose[2] += 1;
                mSQLite.refresh_points(player_name, points_player_lose[0], points_player_lose[1], points_player_lose[2]);
                break;
        }
    }


}