package com.example.graduationprogram;

import android.app.AlertDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.graduationprogram.adapter.LinearAdapter;
import com.example.graduationprogram.database.DatabaseHelper;
import com.example.graduationprogram.enity.User;
import com.example.graduationprogram.enity.UserInfo;
import com.example.graduationprogram.util.Member_array;
import com.example.graduationprogram.util.ToastUtil;

import java.util.ArrayList;

public class MemberActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private AlertDialog alertDialog;
    private DatabaseHelper mSQLite;
    private int sex = 2;
    private View add_member;

    private ArrayList<User> data;
    private ArrayList<UserInfo> memberArray;
    private LinearAdapter linearAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member);

        //创建一个成员数据库
        mSQLite = new DatabaseHelper(this);
        //获取数据库数据，判断用户名是否已存在
        data = mSQLite.getAllDATA();
        //获取性别和姓名数据
        ArrayList<Integer> listSexArray = Member_array.get_sex(data);
        ArrayList<String> listNameArray = Member_array.get_name(data);
        //获取循环视图的xml文件
        RecyclerView mrv = findViewById(R.id.rv_main);
        //创建垂直方向的布局管理器
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        //为循环试图设置布局管理器
        mrv.setLayoutManager(layoutManager);

        memberArray = UserInfo.getDefaultList(listSexArray, listNameArray);

        linearAdapter = new LinearAdapter(MemberActivity.this, new LinearAdapter.OnItemClickListener() {
            @Override
            public void onClick(int pos) {
                Toast.makeText(MemberActivity.this, "click" + pos, Toast.LENGTH_SHORT).show();
            }
        }, new LinearAdapter.OnItemLongClickListener() {
            @Override
            public void onLongClick(int pos) {
                Toast.makeText(MemberActivity.this, "longclick" + pos, Toast.LENGTH_SHORT).show();
            }
        }, memberArray);
        mrv.setAdapter(linearAdapter);

        findViewById(R.id.btn_add_member).setOnClickListener(this);
        //获取工具栏并绑定点击事件
        Toolbar tl_back = findViewById(R.id.tl_back);
        tl_back.setNavigationOnClickListener(view -> {
            finish();
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_add_member:
                //创建对话框建造器
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                LayoutInflater inflater = getLayoutInflater();
                add_member = inflater.inflate(R.layout.add_member, null);
                //获取RadioGroup对象并设置监听
                RadioGroup rg_gender = add_member.findViewById(R.id.rg_gender);
                rg_gender.setOnCheckedChangeListener(this);

                add_member.findViewById(R.id.btn_add_member_s).setOnClickListener(this);
                //自定义对话框布局
                builder.setView(add_member);
                //创建对话框
                alertDialog = builder.create();
                //设置对话框可取消
                alertDialog.setCancelable(true);
                //设置对话框显示位置为界面中间
                Window window = alertDialog.getWindow();
                window.setGravity(Gravity.CENTER);
                alertDialog.show();
                break;

            case R.id.btn_add_member_s:
                EditText et_member_name = add_member.findViewById(R.id.et_member_name);
                //获取输入的成员昵称
                String member_name = et_member_name.getText().toString().trim();

                boolean flag = false;
                for (int i = 0; i < data.size(); i++) {
                    User userdata = data.get(i);
                    if (member_name.equals(userdata.getName())) {
                        flag = true;
                        break;
                    } else {
                        flag = false;
                    }
                }
                //判断用户名是否为空
                if (!TextUtils.isEmpty(member_name)) {
                    if (sex < 2) {
                        if (!flag) {
                            mSQLite.insert(member_name, "111111", sex);
                            //通知适配器发生改变
                            //获取数据库数据
                            data = mSQLite.getAllDATA();
                            //获取性别和姓名数据
                            ArrayList<Integer> listSexArray = Member_array.get_sex(data);
                            ArrayList<String> listNameArray = Member_array.get_name(data);
                            memberArray = UserInfo.getDefaultList(listSexArray, listNameArray);
                            //重新设置适配器参数
                            linearAdapter.setmMemberArray(memberArray);
                            //适配器发生改变
                            linearAdapter.notifyDataSetChanged();
                            alertDialog.dismiss();
                            ToastUtil.show(this, "添加成功!");
                            sex = 2;
                        } else {
                            ToastUtil.show(this, "该成员已存在！");
                        }
                    } else {
                        ToastUtil.show(this, "请选择您的性别");
                    }
                } else {
                    ToastUtil.show(this, "成员昵称不能为空！");
                }
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (i) {
            case R.id.rb_male:
                sex = 1;
                break;
            case R.id.rb_female:
                sex = 0;
                break;
        }
    }
}