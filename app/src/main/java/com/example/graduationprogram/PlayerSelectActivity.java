package com.example.graduationprogram;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.graduationprogram.adapter.PlayerSelectedAdapter;
import com.example.graduationprogram.adapter.PlayerToSelectAdapter;
import com.example.graduationprogram.database.DatabaseHelper;
import com.example.graduationprogram.enity.User;
import com.example.graduationprogram.util.Member_array;
import com.example.graduationprogram.util.ToastUtil;

import java.util.ArrayList;

public class PlayerSelectActivity extends AppCompatActivity implements View.OnClickListener {

    private DatabaseHelper mSQLite;
    private ArrayList<User> data;
    private ArrayList<Integer> listSexSelected;
    private ArrayList<String> listNameSelected;
    private ArrayList<Integer> listSexArray;
    private ArrayList<String> listNameArray;
    private PlayerToSelectAdapter toSelectAdapter;
    private PlayerSelectedAdapter selectedAdapter;
    private Button btn_select_finish;
    private Toolbar tl_back_rotate;
    private String game_name;
    private EditText et_game_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_select);

        tl_back_rotate = findViewById(R.id.tl_back_rotate);
        tl_back_rotate.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        et_game_name = findViewById(R.id.et_game_name);

        ListView player_to_select = findViewById(R.id.player_to_select);
        ListView player_selected = findViewById(R.id.player_selected);
        btn_select_finish = findViewById(R.id.btn_select_finish);
        btn_select_finish.setOnClickListener(this);

        //创建一个成员数据库
        mSQLite = new DatabaseHelper(this);
        //获取数据库数据，判断用户名是否已存在
        data = mSQLite.getAllDATA();
        //获取性别和姓名数据
        listSexArray = Member_array.get_sex(data);
        listNameArray = Member_array.get_name(data);
        //已选择的成员性别和名字
        listSexSelected = Member_array.get_sex(data);
        listNameSelected = Member_array.get_name(data);
        listSexSelected.clear();
        listNameSelected.clear();

        toSelectAdapter = new PlayerToSelectAdapter(listSexArray, listNameArray, PlayerSelectActivity.this, new PlayerToSelectAdapter.OnItemClickListener() {
            @Override
            public void onClick(int pos) {
                //ToastUtil.show(PlayerSelectActivity.this,"点击了可选择第"+pos+"个成员");
                listSexSelected.add(listSexArray.get(pos));
                listNameSelected.add(listNameArray.get(pos));
                listSexArray.remove(pos);
                listNameArray.remove(pos);
                toSelectAdapter.notifyDataSetChanged();
                selectedAdapter.notifyDataSetChanged();
                if (listNameSelected.size() > 3 && listNameSelected.size() < 9) {
                    btn_select_finish.setEnabled(true);
                } else {
                    btn_select_finish.setEnabled(false);
                }
            }
        });

        selectedAdapter = new PlayerSelectedAdapter(listSexSelected, listNameSelected, PlayerSelectActivity.this, new PlayerSelectedAdapter.OnItemClickListener() {
            @Override
            public void onClick(int pos) {
                //ToastUtil.show(PlayerSelectActivity.this,"点击了已选择的第"+pos+"个成员");
                listSexArray.add(listSexSelected.get(pos));
                listNameArray.add(listNameSelected.get(pos));
                listSexSelected.remove(pos);
                listNameSelected.remove(pos);
                selectedAdapter.notifyDataSetChanged();
                toSelectAdapter.notifyDataSetChanged();
                if (listNameSelected.size() > 3 && listNameSelected.size() < 9) {
                    btn_select_finish.setEnabled(true);
                } else {
                    btn_select_finish.setEnabled(false);
                }
            }
        });

        player_to_select.setAdapter(toSelectAdapter);
        player_selected.setAdapter(selectedAdapter);

    }

    @Override
    public void onClick(View v) {
        game_name = et_game_name.getText().toString().trim();
        if (game_name.equals("")){
            ToastUtil.show(PlayerSelectActivity.this,"请输入比赛名称");
            return;
        }

        Intent intent = new Intent();
        intent.setClass(PlayerSelectActivity.this, GameDetailsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("game_name",game_name);
        bundle.putIntegerArrayList("selected_sex", listSexSelected);
        bundle.putStringArrayList("selected_name", listNameSelected);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}