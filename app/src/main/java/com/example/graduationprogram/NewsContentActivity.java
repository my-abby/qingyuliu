package com.example.graduationprogram;

import android.os.Bundle;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.graduationprogram.adapter.NewsAdapter;

import java.util.ArrayList;

public class NewsContentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_content);

        Toolbar tl_back = findViewById(R.id.tl_back);
        tl_back.setNavigationOnClickListener(v -> finish());

        Bundle bundle = this.getIntent().getExtras();
        ArrayList<String> news_content = bundle.getStringArrayList("news_content");
      /*  for (int i = 0; i < news_content.size(); i++) {
            Log.d("jin", "" + news_content.get(i));
        }*/

        ListView layout_content = findViewById(R.id.layout_content);
        layout_content.setAdapter(new NewsAdapter(news_content,this));
    }
}