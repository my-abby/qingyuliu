package com.example.graduationprogram.task;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.graduationprogram.adapter.SingleAdapter;
import com.example.graduationprogram.enity.SingleRanks;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.List;

//通过异步任务类获取数据
public class SingleTask extends AsyncTask {
    private SingleRanks rank_per;
    private ProgressDialog progressDialog;
    private List<SingleRanks> RanksList;
    private SingleAdapter myAdapter;
    private String path;

    public SingleTask(List<SingleRanks> ranksList, SingleAdapter myAdapter, String path) {
        this.RanksList = ranksList;
        this.myAdapter = myAdapter;
        this.path = path;
    }

    //准备执行
    @Override
    protected void onPreExecute() {
        /*progressDialog.show();*/
        //SwipeRefresh.setRefreshing(true);
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        //创建OkHttpClient
        OkHttpClient client = new OkHttpClient();

        //创建请求
        Request request = new Request.Builder()
                .url(path)
                .build();

        //发送请求并处理响应
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
                String html = response.body().string();
                //Log.d("jin","123123"+html);
                // 在这里调用Html解析器，例如Jsoup
                Document doc = Jsoup.parse(html);

                Elements elements = doc.getElementsByTag("tr");
                elements.remove(2);
                elements.remove(1);
                elements.remove(0);
                elements.remove(elements.size() - 3);
                elements.remove(elements.size() - 2);
                elements.remove(elements.size() - 1);
                //Log.d("jin", "12" + elements);
                //Log.d("jin", "elements size:" + elements.size());

                RanksList.clear();
                for (int i = 0; i < elements.size(); i++) {
                    rank_per = new SingleRanks();
                    Elements elements_per = elements.get(i).getElementsByTag("td");
                    int rank = Integer.parseInt(elements_per.get(0).text());
                    rank_per.setRank(rank);
                    //Log.d("jin","12"+rank);

                    String name_country = elements_per.get(1).text();
                    rank_per.setName_country(name_country);
                    //Log.d("jin","12"+name_country);

                    String pic_country = elements_per.get(1).getElementsByTag("img").attr("src");
                    rank_per.setPic_country(pic_country);
                    //Log.d("jin","12"+pic_country);

                    String pic_player = elements_per.get(2).getElementsByTag("img").attr("src");
                    rank_per.setPic_player(pic_player);
                    //Log.d("jin","12"+pic_player);

                    String[] name_player = elements_per.get(2).text().split("\\s+", 2);
                    rank_per.setName_player_ch(name_player[0]);
                    rank_per.setName_player_en(name_player[1]);
                    //Log.d("jin","12"+name_player[0]);

                    String rank_change = elements_per.get(3).text();
                    rank_per.setRank_change(rank_change);
                    //Log.d("jin","12"+rank_change);

                    String rank_up_or_down = elements_per.get(3).getElementsByTag("img").attr("src");
                    rank_per.setRank_up_or_down(rank_up_or_down);
                    //Log.d("jin","12"+rank_up_or_down);

                    int score = Integer.parseInt(elements_per.get(4).text());
                    rank_per.setScore(score);
                    //Log.d("jin","12"+score);
                    RanksList.add(rank_per);
                }
                //Log.d("jin", "RanksList length:" + RanksList.size());
            }
        });
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        //通知适配器发生改变
        myAdapter.notifyDataSetChanged();
        //取消进度条对话框
        //progressDialog.cancel();
    }
}

