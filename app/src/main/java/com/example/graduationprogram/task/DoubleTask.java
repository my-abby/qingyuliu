package com.example.graduationprogram.task;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.graduationprogram.adapter.DoubleAdapter;
import com.example.graduationprogram.enity.DoubleRanks;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.List;

//通过异步任务类获取数据
public class DoubleTask extends AsyncTask {
    private DoubleRanks rank_per;
    private ProgressDialog progressDialog;
    private List<DoubleRanks> RanksList;
    private DoubleAdapter myAdapter;
    private String path;


    public DoubleTask(List<DoubleRanks> ranksList, DoubleAdapter myAdapter, String path) {

        RanksList = ranksList;
        this.myAdapter = myAdapter;
        this.path = path;
    }


    //准备执行
    @Override
    protected void onPreExecute() {
        /*progressDialog.show();*/
        //SwipeRefresh.setRefreshing(true);
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        //首先我们要通过代码从android端通过get或者post方式访问网页，并获取网页的内容
        //path为获取JSON数据的接口

        // 创建OkHttpClient
        OkHttpClient client = new OkHttpClient();

// 创建请求
        Request request = new Request.Builder()
                .url(path)
                .build();

// 发送请求并处理响应
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
                String html = response.body().string();
                //Log.d("jin","123123"+html);
                // 在这里调用Html解析器，例如Jsoup
                Document doc = Jsoup.parse(html);

                Elements elements = doc.getElementsByTag("tr");
                elements.remove(2);
                elements.remove(1);
                elements.remove(0);
                elements.remove(elements.size() - 3);
                elements.remove(elements.size() - 2);
                elements.remove(elements.size() - 1);
                //Log.d("jin", "12" + elements);
                //Log.d("jin", "elements size:" + elements.size());

                RanksList.clear();
                for (int i = 0; i < elements.size(); i++) {
                    rank_per = new DoubleRanks();
                    Elements elements_per = elements.get(i).getElementsByTag("td");
                    int rank = Integer.parseInt(elements_per.get(0).text());
                    rank_per.setRank(rank);
                    //Log.d("jin","12"+rank);

                    String name_country = elements_per.get(1).getElementsByClass("country").get(0).text();
                    rank_per.setName_country(name_country);
                    //Log.d("jin","12"+name_country);

                    String pic_country = elements_per.get(1).getElementsByTag("img").get(0).attr("src");
                    rank_per.setPic_country(pic_country);
                    //Log.d("jin","12"+pic_country);

                    String pic_player_1 = elements_per.get(2).getElementsByClass("player").get(0).getElementsByTag("img").attr("src");
                    rank_per.setPic_player_1(pic_player_1);
                    String pic_player_2 = elements_per.get(2).getElementsByClass("player").get(1).getElementsByTag("img").attr("src");
                    rank_per.setPic_player_2(pic_player_2);
                    //Log.d("jin","12"+pic_player_1);
                    //Log.d("jin","12"+pic_player_2);

                    String[] name_player_1 = elements_per.get(2).getElementsByClass("player").get(0).text().split("\\s+", 2);
                    rank_per.setName_player_ch_1(name_player_1[0]);
                    rank_per.setName_player_en_1(name_player_1[1]);
                    String[] name_player_2 = elements_per.get(2).getElementsByClass("player").get(1).text().split("\\s+", 2);
                    rank_per.setName_player_ch_2(name_player_2[0]);
                    rank_per.setName_player_en_2(name_player_2[1]);
                   /* Log.d("jin","12"+name_player_1[0]);
                    Log.d("jin","12"+name_player_1[1]);
                    Log.d("jin","12"+name_player_2[0]);
                    Log.d("jin","12"+name_player_2[1]);*/

                    String rank_change = elements_per.get(3).text();
                    rank_per.setRank_change(rank_change);
                    //Log.d("jin","12"+rank_change);

                    String rank_up_or_down = elements_per.get(3).getElementsByTag("img").attr("src");
                    rank_per.setRank_up_or_down(rank_up_or_down);
                    //Log.d("jin","12"+rank_up_or_down);

                    int score = Integer.parseInt(elements_per.get(4).text());
                    rank_per.setScore(score);
                    //Log.d("jin","12"+score);
                    RanksList.add(rank_per);

                }
                //Log.d("jin", "RanksList length:" + RanksList.size());
            }
        });
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        //通知适配器发生改变
        myAdapter.notifyDataSetChanged();
        //取消进度条对话框
        //progressDialog.cancel();
    }

}


