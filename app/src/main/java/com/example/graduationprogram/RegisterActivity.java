package com.example.graduationprogram;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;

import com.example.graduationprogram.database.DatabaseHelper;
import com.example.graduationprogram.util.ToastUtil;
import com.example.graduationprogram.enity.User;

import java.util.ArrayList;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private DatabaseHelper mSQLite;
    private EditText et_reg_name;
    private EditText et_reg_password;
    private int sex = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //找到各个控件
        findViewById(R.id.btn_register).setOnClickListener(this);
        findViewById(R.id.btn_back).setOnClickListener(this);
        et_reg_name = findViewById(R.id.et_reg_name);
        et_reg_password = findViewById(R.id.et_reg_password);
        RadioGroup rg_gender = findViewById(R.id.rg_gender);
        rg_gender.setOnCheckedChangeListener(this);

        mSQLite = new DatabaseHelper(RegisterActivity.this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_register:
                //获取输入的用户名和密码
                String name = et_reg_name.getText().toString().trim();
                String password = et_reg_password.getText().toString().trim();

                //获取数据库数据，判断用户名是否已存在
                ArrayList<User> data = mSQLite.getAllDATA();
                boolean flag = false;
                for (int i = 0; i < data.size(); i++) {
                    User userdata = data.get(i);
                    if (name.equals(userdata.getName())) {
                        flag = true;
                        break;
                    } else {
                        flag = false;
                    }
                }
                //判断用户名和密码是否为空
                if (!TextUtils.isEmpty(name) && !TextUtils.isEmpty(password)) {
                    if (sex < 2) {
                        if (!flag) {
                            mSQLite.insert(name, password, sex);
                            finish();
                            ToastUtil.show(this, "注册成功");
                        } else {
                            ToastUtil.show(this, "用户名已被注册");
                            break;
                        }
                    } else {
                        ToastUtil.show(this, "请选择您的性别");
                        break;
                    }
                } else {
                    ToastUtil.show(this, "用户名与密码不能为空");
                    break;
                }
            case R.id.btn_back:
                finish();
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (i) {
            case R.id.rb_male:
                sex = 1;
                break;
            case R.id.rb_female:
                sex = 0;
                break;
        }
    }
}